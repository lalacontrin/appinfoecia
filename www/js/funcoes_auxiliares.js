

function logObject(obj) {
    var ind = "";
    if (arguments.length > 1)
    {
        ind = arguments[1];
    }

    if (typeof obj == "undefined" || obj == null)
    {
        console.log("<null>");
        return;
    }

    if (typeof obj != "object")
    {
        console.log(obj);
        return;
    }

    for (var key in obj)
    {
        if (typeof obj[key] == "object")
        {
            console.log(ind + key + "={");
            logObject(obj[key], ind + "  ");
            console.log(ind + "}");
        } else
        {
            console.log(ind + key + "=" + obj[key]);
        }
    }
}

function alertObject(obj) {
    var ind = "";
    if (arguments.length > 1)
    {
        ind = arguments[1];
    }

    if (typeof obj == "undefined" || obj == null)
    {
        alert("<null>");
        return;
    }

    if (typeof obj != "object")
    {
        alert(obj);
        return;
    }

    for (var key in obj)
    {
        if (typeof obj[key] == "object")
        {
            alert(ind + key + "={");
            alertObject(obj[key], ind + "  ");
            alert(ind + "}");
        } else
        {
            alert(ind + key + "=" + obj[key]);
        }
    }
}

var contCoordenadas = 0;

function pegarCoordenadas() {
   // alert("modalBusca"+modalBusca)
    //console.log("entrou coordenadas")
    if(modalBusca != 1) {
        navigator.geolocation.getCurrentPosition(function (res) {
        app.dialog.close();
        txLatitude = res.coords.latitude;
        txLongitude = res.coords.longitude;
        //logObject(`txLatitude: ${txLatitude}
        //         \n txLongitude: ${txLongitude}`);
//        alertObject(`txLatitude: ${txLatitude}
//                    \n txLongitude: ${txLongitude}`);
    },
            function (e) {
              //alert('eRooGPS');
                //logObject(e);
//                alertObject(e);
                if (cdMotorista) {
                    if (!txLatitude && !txLongitude) {
                        ///alert("123");
                        pegarCoordenadas();
                        app.dialog.create({
                            title: 'Controle de Caixas',
                            text: 'Você precisa ligar o GPS',
                        }).open();
                    }
                }
            }, {timeout: 10000});
          
        }
}

function retornaHojeC() {

    var dataHJ = new Date();

    var mesHJ = dataHJ.getMonth() + 1;
    if (mesHJ < 10) {
        mesHJ = "0" + mesHJ;
    }

    var diaHJ = dataHJ.getDate();
    if (diaHJ < 10) {
        diaHJ = "0" + diaHJ;
    }
    var horaHJ = dataHJ.getHours()
    if (horaHJ < 10) {
        horaHJ = "0" + horaHJ.toString();
    }

    var minutoHJ = dataHJ.getMinutes()
    if (minutoHJ < 10) {
        minutoHJ = "0" + minutoHJ.toString();
    }

    var segundoHJ = dataHJ.getSeconds()
    if (segundoHJ < 10) {
        segundoHJ = "0" + segundoHJ.toString();
    }

    var hoje = dataHJ.getFullYear() + "-" + mesHJ + "-" + diaHJ + " " + horaHJ + ":" + minutoHJ + ":" + segundoHJ;

    return hoje;
}

function addZero(num) {
    if (num < 10) {
        num = "0" + num;
    }
    return num;
}
 
function temInternet() {
    var networkState = navigator.connection.type;
    if (networkState == 'none') {
        return false;
    } else {
        return true;
    }
}

function voltarPagina() {
    modalBusca = 0;
    if (mainView.history.length <= 1) {

        app.dialog.create({
            title: 'Controle de Caixas',
            text: 'Deseja fechar o aplicativo?',
            buttons: [
                {
                    text: 'Não',
                    bold: true,
                    onClick: function () {
                        app.dialog.close();
                    }
                },
                {
                    text: 'Sim',
                    onClick: function () {
                        navigator.app.exitApp();
                    }
                },
            ],
        }).open();

    } else {
        mainView.router.back();
    }

}

function abrirUUID() {
    app.toast.create({
        text: device.uuid,
        position: 'center',
        closeButton: true,
    }).open();
}

function abriVersao(){
    app.toast.create({
        text:"VERSÃO: 2.6",
        position: 'center',
        closeButton: true,
    }).open();
}

function montaTopoLiberacao() {
    if (cdMotorista) {
        $(".page-current #recipTopoLiberacao").html(`<div class="item-subtitle fLeft">Nº Liberação:</div>
                            <div class="item-text fLeft ml16 mt2 fontweightbold">${txLiberacao}</div>
                            <div class="item-subtitle fLeft clear">Veículo:</div>
                            <div class="item-text fLeft ml16 mt2 fontweightbold">${txVeiculo}</div>
                            <div class="item-subtitle clear">Motorista</div>
                            <div class="item-text">${txMotorista}</div>`);
    } else {
        $(".page-current #recipTopoLiberacao").html(`<div class="item-subtitle">Promotor</div>
                            <div class="item-text">${txPromotor}</div>`);
    }
}

function abrirAvisos(txt, classAviso, flvoltar) {
    app.toast.create({
        text: txt,
        closeButton: true,
        closeButtonColor: 'white',
        on: {
            close: function () {
                if (flvoltar) {
                    voltarPagina();
                }
            },
        }
    }).open();
    $(".toast").addClass(classAviso)
}

function abreModalSemInternet() {
    app.toast.create({
        icon: '<img class="icon" src="img/wifi_off.png" alt="Sem Internet"/>',
        text: 'Sem Internet',
        position: 'center',
        closeTimeout: 2000,
        closeButton: true,
        closeButtonColor: 'white'
    }).open();
}