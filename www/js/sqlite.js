function zerarBancodeDadosDB(cdmotorista, txmotorista) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql('DELETE FROM motorista ', [], function (tx, res) {
                    tx.executeSql('DELETE FROM liberacao ', [], function (tx, res) {
                        tx.executeSql('DELETE FROM cliente ', [], function (tx, res) {
                            tx.executeSql('DELETE FROM clienteembalagem ', [], function (tx, res) {
                                tx.executeSql('DELETE FROM clienteembalagementrada ', [], function (tx, res) {
                                    tx.executeSql('DELETE FROM embalagem ', [], function (tx, res) {
                                        tx.executeSql('DELETE FROM liberacoes ', [], function (tx, res) {
                                            tx.executeSql('DELETE FROM transferencia_caminhao ', [], function (tx, res) {
                                                tx.executeSql('DELETE FROM promotor ', [], function (tx, res) {
                                                    tx.executeSql('DELETE FROM transferencia_historico ', [], function (tx, res) {
                                                        tx.executeSql('DROP TABLE motorista ');
                                                        tx.executeSql('DROP TABLE promotor ');
                                                        tx.executeSql('DROP TABLE embalagem ');
                                                        tx.executeSql('DROP TABLE liberacao ');
                                                        tx.executeSql('DROP TABLE liberacaoembalagem ');
                                                        tx.executeSql('DROP TABLE cliente ');
                                                        tx.executeSql('DROP TABLE clienteembalagem ');
                                                        tx.executeSql('DROP TABLE clienteembalagementrada ');
                                                        tx.executeSql('DROP TABLE liberacoes ');
                                                        tx.executeSql('DROP TABLE transferencia_caminhao ');
                                                        tx.executeSql('DROP TABLE transferencia_historico ');

                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS motorista (cdmotorista INTEGER NOT NULL, txmotorista VARCHAR(255))');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS promotor (cdpromotor INTEGER NOT NULL, txpromotor VARCHAR(255))');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS embalagem (cdembalagem INTEGER NOT NULL, txembalagem VARCHAR(255), vlcxvazia INTEGER)');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS liberacao (cdliberacao INTEGER NOT NULL, cdmotorista INTEGER, txmotorista VARCHAR(255), cdveiculo INTEGER, txveiculo VARCHAR(255), txliberacao VARCHAR(2))');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS liberacaoembalagem (cdliberacao INTEGER NOT NULL, cdembalagem INTEGER, txembalagem VARCHAR(255), vlcaixascheias INTEGER, vlcaixasvazias INTEGER)');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS cliente (cdcliente INTEGER NOT NULL, txcliente VARCHAR(255), txrazaosocial VARCHAR(255), txcidade VARCHAR(255), txuf VARCHAR(2))');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS clienteembalagem (cdclienteembalagem INTEGER NOT NULL, cdcliente INTEGER, cdembalagem INTEGER, txembalagem VARCHAR(255), vlsaldo INTEGER, flsincronizado INTEGER)');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS clienteembalagementrada (cdclienteembalagementrada INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, cdcliente INTEGER, cdembalagem INTEGER, vlentrada VARCHAR(255), vlsaida VARCHAR(255), dtentrada INTEGER, flsincronizado INTEGER, txobs VARCHAR(255), txlatitude VARCHAR(255), txlongitude VARCHAR(255))');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS liberacoes (cdliberacao INTEGER, cdveiculo INTEGER, txveiculo VARCHAR(255), cdmotorista INTEGER, txmotorista VARCHAR(255))');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS transferencia_caminhao (cdmotorista INTEGER, cdliberacao INTEGER, cdembalagem INTEGER, dttransferir DATETIME, vlquantidade INTEGER)');
                                                        tx.executeSql('CREATE TABLE IF NOT EXISTS transferencia_historico (cdtransferencia INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, dttransferencia DATETIME, cdembalagem INTEGER, txembalagem VARCHAR(255), vlquantidade INTEGER, txmotorista VARCHAR(255), txveiculo VARCHAR(255), fltipo INTEGER, cdliberacao INTEGER)');

                                                        tx.executeSql('DELETE FROM motorista ');
                                                        tx.executeSql('DELETE FROM promotor ');
                                                        tx.executeSql('DELETE FROM embalagem ');
                                                        tx.executeSql('DELETE FROM liberacao ');
                                                        tx.executeSql('DELETE FROM liberacaoembalagem ');
                                                        tx.executeSql('DELETE FROM cliente ');
                                                        tx.executeSql('DELETE FROM clienteembalagem ');
                                                        tx.executeSql('DELETE FROM clienteembalagementrada ');
                                                        tx.executeSql('DELETE FROM liberacoes ');
                                                        tx.executeSql('DELETE FROM transferencia_caminhao ');
                                                        tx.executeSql('DELETE FROM transferencia_historico ');

                                                    }, function (jsonError) {
                                                        //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                                        //logObject(jsonError);
                                                    });

                                                }, function (jsonError) {
                                                    //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                                    //logObject(jsonError);
                                                });

                                            }, function (jsonError) {
                                                //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                                //logObject(jsonError);
                                            });

                                        }, function (jsonError) {
                                            //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                            //logObject(jsonError);
                                        });
                                    }, function (jsonError) {
                                        //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                        //logObject(jsonError);
                                    });
                                }, function (jsonError) {
                                    //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                    //logObject(jsonError);
                                });
                            }, function (jsonError) {
                                //console.log("Error while drop and  creating table clienteembalagementrada: ");
                                //logObject(jsonError);
                            });
                        }, function (jsonError) {
                            //console.log("Error while drop and  creating table clienteembalagementrada: ");
                            //logObject(jsonError);
                        });
                    }, function (jsonError) {
                        //console.log("Error while drop and  creating table clienteembalagem: ");
                        //logObject(jsonError);
                    });
                }, function (jsonError) {
                    //console.log("Error while drop and  creating table cliente: ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while drop and  creating table : ");
        //logObject(jsonError);
    }, function () {
        //console.log("DB Destroy successfully");
        if (cdmotorista && txmotorista) {
            cdMotorista = cdmotorista;
            txMotorista = txmotorista;
            insereMotorista(cdMotorista, txMotorista);
            location.reload();
        } else {
            location.reload();
        }
    });
}

function createDB() {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS motorista (cdmotorista INTEGER NOT NULL, txmotorista VARCHAR(255))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS promotor (cdpromotor INTEGER NOT NULL, txpromotor VARCHAR(255))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS embalagem (cdembalagem INTEGER NOT NULL, txembalagem VARCHAR(255), vlcxvazia INTEGER)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS liberacao (cdliberacao INTEGER NOT NULL, cdmotorista INTEGER, txmotorista VARCHAR(255), cdveiculo INTEGER, txveiculo VARCHAR(255), txliberacao VARCHAR(2))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS liberacaoembalagem (cdliberacao INTEGER NOT NULL, cdembalagem INTEGER, txembalagem VARCHAR(255), vlcaixascheias INTEGER, vlcaixasvazias INTEGER)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS cliente (cdcliente INTEGER NOT NULL, txcliente VARCHAR(255), txrazaosocial VARCHAR(255), txcidade VARCHAR(255), txuf VARCHAR(2))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS clienteembalagem (cdclienteembalagem INTEGER NOT NULL, cdcliente INTEGER, cdembalagem INTEGER, txembalagem VARCHAR(255), vlsaldo INTEGER, flsincronizado INTEGER)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS clienteembalagementrada (cdclienteembalagementrada INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, cdcliente INTEGER, cdembalagem INTEGER, vlentrada VARCHAR(255), vlsaida VARCHAR(255), dtentrada INTEGER, flsincronizado INTEGER, txobs VARCHAR(255), txlatitude VARCHAR(255), txlongitude VARCHAR(255))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS liberacoes (cdliberacao INTEGER, cdveiculo INTEGER, txveiculo VARCHAR(255), cdmotorista INTEGER, txmotorista VARCHAR(255))');
                tx.executeSql('CREATE TABLE IF NOT EXISTS transferencia_caminhao (cdmotorista INTEGER, cdliberacao INTEGER, cdembalagem INTEGER, dttransferir DATETIME, vlquantidade INTEGER)');
                tx.executeSql('CREATE TABLE IF NOT EXISTS transferencia_historico (cdtransferencia INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, dttransferencia DATETIME, cdembalagem INTEGER, txembalagem VARCHAR(255), vlquantidade INTEGER, txmotorista VARCHAR(255), txveiculo VARCHAR(255), fltipo INTEGER, cdliberacao INTEGER)');
            }, function (jsonError) {
//        console.log("Error while creating table : ");
//        logObject(jsonError);
    }, function () {
//        console.log("DB Created successfully DB");
    });
}

function verificaLogado() {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT *
                              FROM motorista LIMIT 1 `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                        cdMotorista = res.rows.item(0).cdmotorista;
                        txMotorista = res.rows.item(0).txmotorista;

                        retornaClientes();
                        retornaEmbalagens();
                        retornaLiberacoes();

                        tx.executeSql(`SELECT *
                              FROM liberacao LIMIT 1 `, [], function (tx, res) {
                            if (res.rows.length > 0) {
                                arrLiberacao['cdliberacao'] = res.rows.item(0).cdliberacao;
                                cdliberacao = res.rows.item(0).cdliberacao;
                                txLiberacao = res.rows.item(0).cdliberacao;
                                txVeiculo = res.rows.item(0).txveiculo;

                                if (txLiberacao) {
                                    retornaHistoricoTransferenciasLiberacao();
                                }

                                tx.executeSql(`SELECT * FROM liberacaoembalagem WHERE cdliberacao = ${arrLiberacao['cdliberacao']} `, [],
                                        function (tx, res) {
                                            if (res.rows.length > 0) {
                                                arrLiberacao['embalagens'] = [];
                                                for (var i = 0; i < res.rows.length; i++) {
                                                    arrLiberacao['embalagens'][i] = res.rows.item(i);
                                                }
                                            }

                                            app.dialog.close();
                                            $(".menusair").removeClass('hide');
                                            mainView.router.navigate('/inicial', {
                                                reloadCurrent: true, //troca a pagina atual por essa remove do historico
                                                reloadPrevious: false, // troca a anterior por essa
                                                reloadAll: false // zera a rota e começa de novo, usar no logout
                                            })
                                        }, function (jsonError) {
                                    //console.log("Error while select logado --- : ");
                                    //logObject(jsonError);
                                })
                            } else {
                                app.dialog.close();
                                $(".menusair").removeClass('hide');
                                mainView.router.navigate('/carregar-dados', {
                                    reloadCurrent: true, //troca a pagina atual por essa remove do historico
                                    reloadPrevious: false, // troca a anterior por essa
                                    reloadAll: false // zera a rota e começa de novo, usar no logout
                                })
                            }
                        }, function (jsonError) {
                            //console.log("Error while select logado --- : ");
                            //logObject(jsonError);
                        })
                    } else {
                        tx.executeSql(`SELECT *
                              FROM promotor LIMIT 1 `, [], function (tx, res) {
                            if (res.rows.length > 0) {
                                cdPromotor = res.rows.item(0).cdpromotor;
                                txPromotor = res.rows.item(0).txpromotor;
                                retornaEmbalagens();
                                retornaClientes(1);
                            }
                        }, function (jsonError) {
                            app.dialog.close();
                            //console.log("*Error while select logado : ");
                            //logObject(jsonError);
                        })
                        app.dialog.close();
                    }
                }, function (jsonError) {
                    app.dialog.close();
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        app.dialog.close();
        //console.log("**Error while conecta table sysclientelogado : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function retornaClientes(fltipo) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT *
                              FROM cliente `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            arrClientes[i] = res.rows.item(i);
                        }
                        if (arrClientes) {
                            for (var y = 0; y < arrClientes.length; y++) {
                                retornaClientesEmbalagens(y, res.rows.length);
                            }
                        }
                        if (fltipo == 1) {
                            app.dialog.close();
                            $(".menusair").removeClass('hide');
                            mainView.router.navigate('/lista-clientes', {
                                reloadCurrent: true, //troca a pagina atual por essa remove do historico
                                reloadPrevious: false, // troca a anterior por essa
                                reloadAll: false // zera a rota e começa de novo, usar no logout
                            })
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table sysclientelogado : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function retornaClientesEmbalagens(y, tamY) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT *
                              FROM clienteembalagem 
                              WHERE cdcliente = ${arrClientes[y].cdcliente}  `, [], function (tx, ress) {
                    if (ress.rows.length > 0) {
                        arrClientes[y]['embalagens'] = [];
                        for (var x = 0; x < ress.rows.length; x++) {
                            arrClientes[y]['embalagens'][x] = ress.rows.item(x);
                            retornaClienteEmbalagemEntradaCliente(y, tamY, x, ress.rows.length)
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table sysclientelogado : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function retornaClienteEmbalagemEntradaCliente(y, tamY, x, tamX) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM clienteembalagementrada
                               WHERE cdcliente = "${arrClientes[y].cdcliente}" AND 
                               cdembalagem = "${arrClientes[y]['embalagens'][x].cdembalagem}" `, [], function (tx, ress) {
                    if (ress.rows.length > 0) {
                        arrClientes[y]['embalagens'][x].vlentrada = ress.rows.item(0).vlentrada;
                        arrClientes[y]['embalagens'][x].vlsaida = ress.rows.item(0).vlsaida;
                        arrClientes[y]['embalagens'][x].txobs = ress.rows.item(0).txobs;
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while inserir table clienteembalagementrada : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}

function insereMotorista(cdmotorista, txmotorista) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`INSERT INTO motorista 
                              (cdmotorista, txmotorista) 
                               VALUES 
                              ("${cdmotorista}", "${txmotorista}")`);
            }, function (jsonError) {
        //console.log("Error while inserir table motorista : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}

function inserePromotor(cdpromotor, txpromotor) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`INSERT INTO promotor 
                              (cdpromotor, txpromotor) 
                               VALUES 
                              ("${cdpromotor}", "${txpromotor}")`);
            }, function (jsonError) {
        //console.log("Error while inserir table promotor : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}

function deletaEmbalagem(arrEmbalagens) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`DELETE FROM embalagem  `, [], function (tx, res) {
//                    //logObject(arrLiberacoes)
                    if (arrEmbalagens[0]) {
                        for (e in arrEmbalagens) {
                            insereEmbalagem(arrEmbalagens[e].cdembalagem, arrEmbalagens[e].txembalagem, arrEmbalagens[e].vlcxvazia)
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select liberacoes : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table liberacoes : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully liberacoes");
    });
}

function insereEmbalagem(cdembalagem, txembalagem, vlcxvazia) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM embalagem WHERE cdembalagem = ${cdembalagem} `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                    } else {
                        tx.executeSql(`INSERT INTO embalagem 
                              (cdembalagem, txembalagem, vlcxvazia) 
                               VALUES 
                              ("${cdembalagem}", "${txembalagem}", "${vlcxvazia}")`, [], function (tx, res) {
                        }, function (jsonError) {
                            //console.log("*Error while select embalagem : ");
                            //logObject(jsonError);
                        });
                    }
                }, function (jsonError) {
                    //console.log("*Error while select embalagem : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table embalagem : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function updateEmbalagemVazia(cdembalagem, vlcxvazia) {
    //console.log(`UPDATE embalagem 
//                              SET vlcxvazia = ${vlcxvazia}
    //   WHERE cdembalagem = ${cdembalagem}`)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`UPDATE embalagem 
                              SET vlcxvazia = ${vlcxvazia}
                               WHERE cdembalagem = ${cdembalagem}`);
            }, function (jsonError) {
        //console.log("Error while inserir table embalagem : ");
        //logObject(jsonError);
    }, function () {
        //console.log("DB Created successfully embalagem");
    });
}

function retornaEmbalagens() {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM embalagem  `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                        for (e = 0; e < res.rows.length; e++) {
                            arrEmbalagens[e] = res.rows.item(e);
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table sysclientelogado : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereTransferenciaHistorico(dttransferencia, cdembalagem, txembalagem, vlquantidade, txmotorista, txveiculo, fltipo, cdliberacao) {

    new Date(dttransferencia)
    //logObject(`INSERT INTO transferencia_historico 
    //  (dttransferencia, cdembalagem, txembalagem, vlquantidade, txmotorista, txveiculo, fltipo, cdliberacao) 
    // VALUES 
    //   ("${dttransferencia}", "${cdembalagem}", "${txembalagem}", "${vlquantidade}", "${txmotorista}", "${txveiculo}", "${fltipo}", "${cdliberacao}")`)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`INSERT INTO transferencia_historico 
                              (dttransferencia, cdembalagem, txembalagem, vlquantidade, txmotorista, txveiculo, fltipo, cdliberacao) 
                               VALUES 
                              ("${dttransferencia}", "${cdembalagem}", "${txembalagem}", "${vlquantidade}", "${txmotorista}", "${txveiculo}", "${fltipo}", "${cdliberacao}")`);
            }, function (jsonError) {
        //console.log("Error while inserir table motorista : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}


function deletaTransferenciaHistorico(arrTransff) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`DELETE FROM transferencia_historico  `, [], function (tx, res) {
                    if (arrTransff[0]) {
                        for (e in arrTransff) {
                            insereTransferenciaHistorico(arrTransff[e].dttransferencia, arrTransff[e].cdembalagem, arrTransff[e].txembalagem, arrTransff[e].vlquantidade, arrTransff[e].txmotorista, arrTransff[e].txembalagem, arrTransff[e].txveiculo, arrTransff[e].fltipo, arrTransff[e].cdliberacao)
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select liberacoes : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table liberacoes : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully liberacoes");
    });
}

function retornaTransferenciaHistoricoTipo(fltipo) {
    //console.log(`SELECT * FROM transferencia_historico WHERE fltipo = '${fltipo}' `)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM transferencia_historico WHERE fltipo = '${fltipo}' `, [], function (tx, res) {

                    //console.log("TAMANHO: " + res.rows.length);
                    if (res.rows.length > 0) {
                        for (e = 0; e < res.rows.length; e++) {
                            arr[e] = res.rows.item(e);
                        }
                        montaHtmlTranferencia(arr)
                    } else {
                        montaHtmlTranferencia('')
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table sysclientelogado : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereLiberacoes(cdliberacao, cdveiculo, txveiculo, cdmotorista, txmotorista) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM liberacoes WHERE cdliberacao = ${cdliberacao} `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                    } else {
                        tx.executeSql(`INSERT INTO liberacoes 
                              (cdliberacao, cdveiculo, txveiculo, cdmotorista, txmotorista) 
                               VALUES 
                              ("${cdliberacao}", "${cdveiculo}", "${txveiculo}", "${cdmotorista}", "${txmotorista}")`, [], function (tx, res) {
                        }, function (jsonError) {
                            //console.log("*Error while select liberacoes : ");
                            //logObject(jsonError);
                        });

                    }
                }, function (jsonError) {
                    //console.log("*Error while select liberacoes : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table liberacoes : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function deletaLiberacoes(arrLiberacoes) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`DELETE FROM liberacoes  `, [], function (tx, res) {
//                    //logObject(arrLiberacoes)
                    if (arrLiberacoes[0]) {
                        for (c in arrLiberacoes) {
                            insereLiberacoes(arrLiberacoes[c].cdliberacao, arrLiberacoes[c].cdveiculo, arrLiberacoes[c].txveiculo, arrLiberacoes[c].cdmotorista, arrLiberacoes[c].txmotorista);
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select liberacoes : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table liberacoes : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully liberacoes");
    });
}

function retornaLiberacoes() {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM liberacoes  `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                        for (l = 0; l < res.rows.length; l++) {
                            arrLiberacoes[e] = res.rows.item(l);
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table sysclientelogado : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereCaminhao(cdmotorista, cdliberacao, cdembalagem, dttransferir, vlquantidade) {
    new Date(dttransferir)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`INSERT INTO transferencia_caminhao 
                              (cdmotorista, cdliberacao, cdembalagem, dttransferir, vlquantidade) 
                               VALUES 
                              ("${cdmotorista}", "${cdliberacao}", "${cdembalagem}", "${dttransferir}", "${vlquantidade}")`);
            }, function (jsonError) {
        //console.log("Error while inserir table caminhao : ");
        // alert("Erro\n\n "+JSON.stringify(jsonError))
        //logObject(jsonError);
    }, function () {
        //console.log("DB Created successfully caminhao");
    });
}

function retornaCaminhao(cdliberacao, tipo) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM transferencia_caminhao  `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                        for (c = 0; c < res.rows.length; c++) {
                            //sincroniza;
                            arrSincronizarTCaminhoes[c] = res.rows.item(c);
                            inseretCaminhoesTransferencia(cdliberacao, c, res.rows.length, tipo);
                        }
                    } else {
                        if (tipo == 1 || tipo == 3) {
                            retornaClientesSaldoAtualiza(tipo);
                            //
                        } else {
                            atualizaSituacaoLiberacao(cdliberacao);
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select caminhao123 : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table caminhao 345 : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully caminhao 10");
    });
}

function deletaCaminhaoTransferencia(cdmotorista, cdliberacao, cdembalagem, dttransferir, vlquantidade) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`DELETE FROM transferencia_caminhao 
/                                WHERE cdmotorista = '${cdmotorista}' AND cdliberacao = '${cdliberacao}' AND  
                                cdembalagem = '${cdembalagem}' AND  dttransferir = '${dttransferir}' AND  
                                vlquantidade = '${vlquantidade}' `, [], function (tx, res) {


                }, function (jsonError) {
                    //console.log("*Error while select transferencia_caminhao24 : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table transferencia_caminhao9 : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereLiberacao(cdliberacao, cdmotorista, txmotorista, cdveiculo, txveiculo, txliberacao) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM liberacao WHERE cdliberacao = ${cdliberacao} `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                    } else {
                        tx.executeSql(`INSERT INTO liberacao 
                              (cdliberacao, cdmotorista, txmotorista, cdveiculo, txveiculo, txliberacao) 
                               VALUES 
                              ("${cdliberacao}", "${cdmotorista}", "${txmotorista}", "${cdveiculo}", "${txveiculo}", "${txliberacao}")`, [], function (tx, res) {
                        }, function (jsonError) {
                            //console.log("*Error while select liberacao : ");
                            //logObject(jsonError);
                        });

                    }
                }, function (jsonError) {
                    //console.log("*Error while select liberacao : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table liberacao : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereLiberacaoEmbalagem(cdliberacao, cdembalagem, txembalagem, vlcaixascheias, vlcaixasvazias) {
//    console.log(`INSERT INTO liberacaoembalagem 
//                              (cdliberacao, cdembalagem, txembalagem, vlcaixascheias, vlcaixasvazias) 
//                               VALUES 
//                              ("${cdliberacao}", "${cdembalagem}", "${txembalagem}", "${vlcaixascheias}", "${vlcaixasvazias}" )`)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM liberacaoembalagem WHERE cdliberacao = ${cdliberacao} AND cdembalagem = ${cdembalagem}`, [], function (tx, res) {
                    if (res.rows.length > 0) {
                    } else {
                        tx.executeSql(`INSERT INTO liberacaoembalagem 
                              (cdliberacao, cdembalagem, txembalagem, vlcaixascheias, vlcaixasvazias) 
                               VALUES 
                              ("${cdliberacao}", "${cdembalagem}", "${txembalagem}", "${vlcaixascheias}", "${vlcaixasvazias}" )`, [], function (tx, res) {
                        }, function (jsonError) {
                            //console.log("*Error while select liberacaoembalagem : ");
                            //logObject(jsonError);
                        });

                    }
                }, function (jsonError) {
                    //console.log("*Error while select liberacao : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table liberacao : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function deleteTodosClientes(arrClientes) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`DELETE FROM cliente `, [],
                        function (tx, ress) {
                            //console.log("kkkkkkkkkkkkkkkkkkkkkkkk22")
                            tx.executeSql(`DELETE FROM clienteembalagem `, [],
                                    function (tx, ress) {
                                        //console.log("kkkkkkkkkkkkkkkkkkkkkkkk333")
                                        tx.executeSql(`DELETE FROM clienteembalagementrada `, [],
                                                function (tx, ress) {
                                                    //console.log("kkkkkkkkkkkkkkkkkkkkkkkk")
                                                    intervalSinc = setInterval(function () {
                                                        if (cdMotorista || cdPromotor) {
                                                            retornaClienteEmbalagemEntrada();
                                                        }
                                                    }, 1000);
                                                    intervalCoord = setInterval(function () {
                                                        pegarCoordenadas();
                                                    }, 60000);
                                                    createDB();
                                                    dtentrada = retornaHojeC();
                                                    if (arrClientes) {
                                                        for (x in arrClientes) {
                                                            insereCliente(arrClientes[x].cdcliente, arrClientes[x].txcliente, arrClientes[x].txrazaosocial, arrClientes[x].txcidade, arrClientes[x].txuf);
                                                            if (arrClientes[x].embalagens) {
                                                                for (y in arrClientes[x].embalagens) {
                                                                    insereClienteEmbalagem(arrClientes[x].embalagens[y].cdclienteembalagem, arrClientes[x].cdcliente, arrClientes[x].embalagens[y].cdembalagem, arrClientes[x].embalagens[y].txembalagem, arrClientes[x].embalagens[y].vlsaldo, 1, dtentrada, '');
                                                                }
                                                            }
                                                        }
                                                    }
                                                }, function (jsonError) {
                                            //console.log("*Error while select deleteTodosClientes : ");
                                            //logObject(jsonError);
                                        });
                                    }, function (jsonError) {
                                //console.log("*Error while select deleteTodosClientes : ");
                                //logObject(jsonError);
                            });
                        }, function (jsonError) {
                    //console.log("*Error while select deleteTodosClientes : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while inserir table deleteTodosClientes : ");
        //logObject(jsonError);
    }, function () {
        //console.log("DB Created successfully deleteTodosClientes");
    });
}

function retornaInsereClientes(arrCliente, x, tamX, txbusca) {
//    console.log(`*****${arrCliente.cdcliente}*****${x}******`)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT cdcliente FROM cliente WHERE cdcliente = ${arrCliente.cdcliente} `, [],
                        function (tx, ress) {
                            dtentrada = retornaHojeC();
                            //console.log(`SELECT cdcliente FROM cliente WHERE cdcliente = ${arrCliente.cdcliente} `);
                            //logObject(ress.rows.item(0));
                            //logObject(ress.rows.length);
                            if (ress.rows.length > 0) {
                                //console.log('update')
                                //update
                                if (arrClientes) {
                                    for (y in arrClientes) {
                                        if (arrClientes[y].cdcliente == arrCliente.cdcliente) {
                                            updateCliente(arrCliente.cdcliente, arrCliente.txcliente, arrCliente.txrazaosocial, arrCliente.txcidade, arrCliente.txuf);
                                            if (arrClientes[y].embalagens) {
                                                for (z in arrClientes[y].embalagens) {
                                                    flTem = 0;
                                                    if (arrCliente.embalagens) {
                                                        for (t in arrCliente.embalagens) {
                                                            if (arrClientes[y].embalagens[z].cdembalagem == arrCliente.embalagens[t].cdembalagem) {
                                                                updateSaldoClienteEmbalagem(arrCliente.cdcliente, arrCliente.embalagens[t].cdembalagem, arrCliente.embalagens[t].vlsaldo);
                                                                flTem == 1;
                                                            }
                                                        }
                                                    }
                                                    if (!flTem) {
                                                        updateSaldoClienteEmbalagem(arrCliente.cdcliente, arrClientes[y].embalagens[z].cdembalagem, 0);
                                                    }
                                                }
                                            }
                                            //console.log('yyyyyyy:' + y)
                                            arrClientes[y] = arrCliente;
                                        }
                                    }
                                }
                            } else {
                                //console.log('insere')
                                //insere
                                arrClientes[arrClientes.length] = arrCliente;
                                insereCliente(arrCliente.cdcliente, arrCliente.txcliente, arrCliente.txrazaosocial, arrCliente.txcidade, arrCliente.txuf);
                                if (arrCliente.embalagens) {
                                    for (y in arrCliente.embalagens) {
                                        insereClienteEmbalagem(arrCliente.embalagens[y].cdclienteembalagem, arrCliente.cdcliente, arrCliente.embalagens[y].cdembalagem, arrCliente.embalagens[y].txembalagem, arrCliente.embalagens[y].vlsaldo, 1, dtentrada, '');
                                    }
                                }
                            }
                            if (x == tamX - 1) {
                                app.dialog.close();
                                if (txbusca) {
                                    mainView.router.navigate('/lista-clientes/' + txbusca, {
                                        reloadCurrent: true, //troca a pagina atual por essa remove do historico
                                        reloadPrevious: false, // troca a anterior por essa
                                        reloadAll: false // zera a rota e começa de novo, usar no logout
                                    })
                                }
                            }
                        }, function (jsonError) {
                    //console.log("*Error while select deleteTodosClientes : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while inserir table deleteTodosClientes : ");
        //logObject(jsonError);
    }, function () {
        //console.log("DB Created successfully deleteTodosClientes");
    });
}

function insereCliente(cdcliente, txcliente, txrazaosocial, txcidade, txuf) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM cliente WHERE cdcliente = ${cdcliente}  `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                    } else {
                        tx.executeSql(`INSERT INTO cliente 
                              (cdcliente, txcliente, txrazaosocial, txcidade, txuf) 
                               VALUES 
                              ("${cdcliente}", "${txcliente}", "${txrazaosocial}", "${txcidade}", "${txuf}")`, [], function (tx, res) {
                        }, function (jsonError) {
                            //console.log("*Error while select cliente : ");
                            //logObject(jsonError);
                        });

                    }
                }, function (jsonError) {
                    //console.log("*Error while select cliente : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table cliente : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function updateCliente(cdcliente, txcliente, txrazaosocial, txcidade, txuf) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`UPDATE cliente SET txcliente = '${txcliente}',
                              txrazaosocial = "${txrazaosocial}", txcidade = "${txcidade}", 
                              txuf = "${txuf}" WHERE cdliente = "${cdcliente}" `, [], function (tx, res) {
                }, function (jsonError) {
                    //console.log("*Error while select cliente : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("**Error while conecta table cliente : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereClienteEmbalagem(cdclienteembalagem, cdcliente, cdembalagem, txembalagem, vlsaldo, flsincronizado, dtentrada) {
//console.log(`INSERT INTO clienteembalagem 
//                              (cdclienteembalagem, cdcliente, cdembalagem, txembalagem, vlsaldo, flsincronizado) 
//                               VALUES 
//                              ("${cdclienteembalagem}", "${cdcliente}", "${cdembalagem}", "${txembalagem}", "${vlsaldo}", "${flsincronizado}")`)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM clienteembalagem WHERE cdcliente = ${cdcliente} AND cdembalagem = ${cdembalagem} `, [], function (tx, res) {
                    if (res.rows.length > 0) {
                    } else {
                        vlsaldo = Number(vlsaldo);
                        tx.executeSql(`INSERT INTO clienteembalagem 
                              (cdclienteembalagem, cdcliente, cdembalagem, txembalagem, vlsaldo, flsincronizado) 
                               VALUES 
                              ("${cdclienteembalagem}", "${cdcliente}", "${cdembalagem}", "${txembalagem}", "${vlsaldo}", "${flsincronizado}")`, [],
                                function (tx, ress) {
                                    insereClienteEmbalagemEntrada(cdcliente, cdembalagem, 0, 0, dtentrada, 0, '');
                                }, function (jsonError) {
                            //console.log("*Error while select clienteembalagem : ");
                            //logObject(jsonError);
                        });

                    }
                }, function (jsonError) {
                    //console.log("*Error while select clienteembalagem : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table clienteembalagem : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereSomenteClienteEmbalagem(cdclienteembalagem, cdcliente, cdembalagem, txembalagem, vlsaldo, flsincronizado) {

//    console.log(`SELECT * FROM clienteembalagem WHERE cdcliente = ${cdcliente} AND cdembalagem = ${cdembalagem} `)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM clienteembalagem WHERE cdcliente = ${cdcliente} AND cdembalagem = ${cdembalagem} `, [], function (tx, res) {
                    if (res.rows.length > 0) {
//                        console.log("if")
                    } else {
//                        console.log("else")
//                        console.log(`INSERT INTO clienteembalagem 
//                              (cdclienteembalagem, cdcliente, cdembalagem, txembalagem, vlsaldo, flsincronizado) 
//                               VALUES 
//                              ("${cdclienteembalagem}", "${cdcliente}", "${cdembalagem}", "${txembalagem}", "${vlsaldo}", "${flsincronizado}")`)
                        vlsaldo = Number(vlsaldo);
                        tx.executeSql(`INSERT INTO clienteembalagem 
                              (cdclienteembalagem, cdcliente, cdembalagem, txembalagem, vlsaldo, flsincronizado) 
                               VALUES 
                              ("${cdclienteembalagem}", "${cdcliente}", "${cdembalagem}", "${txembalagem}", "${vlsaldo}", "${flsincronizado}")`, [],
                                function (tx, ress) {
                                }, function (jsonError) {
                            //console.log("*Error while select clienteembalagem :e14 ");
                            //logObject(jsonError);
                        });
                    }
                }, function (jsonError) {
                    //console.log("*Error while select clienteembalagem : ");
                    //logObject(jsonError);
                })
            }, function (jsonError) {
        //console.log("**Error while conecta table clienteembalagem : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function updateSaldoClienteEmbalagem(cdcliente, cdembalagem, vlsaldo) {
//    console.log(`UPDATE clienteembalagem SET  vlsaldo = "${vlsaldo}"
//                                WHERE  cdcliente = ${cdcliente} AND cdembalagem = ${cdembalagem} `)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                vlsaldo = Number(vlsaldo);
                tx.executeSql(`UPDATE clienteembalagem SET  vlsaldo = "${vlsaldo}"
                                WHERE  cdcliente = ${cdcliente} AND cdembalagem = ${cdembalagem} `, [],
                        function (tx, ress) {
                        }, function (jsonError) {
                    //console.log("*Error while select clienteembalagem :e14 ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("**Error while conecta table clienteembalagem : ");
        //logObject(jsonError);
    }, function () {
        //console.log("conexao successfully");
    });
}

function insereClienteEmbalagemEntrada(cdcliente, cdembalagem, vlentrada, vlsaida, dtentrada, flsincronizado, txobs) {
    new Date(dtentrada)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT * FROM clienteembalagementrada WHERE cdcliente = ${cdcliente} AND cdembalagem = ${cdembalagem} `, [],
                        function (tx, res) {
                            if (res.rows.length > 0) {
                                // alert("aki")
                            } else {
//                                console.log(`INSERT INTO clienteembalagementrada 
//                                               (cdcliente, cdembalagem, vlentrada, vlsaida, dtentrada, 
//                                                flsincronizado, txobs, txlatitude, txlongitude) 
//                                                VALUES ("${cdcliente}", "${cdembalagem}", "${vlentrada}", 
//                                                "${vlsaida}", "${dtentrada}", "${flsincronizado}", "${txobs}", 
//                                                "${txLatitude}", "${txLongitude}")`)
                                tx.executeSql(`INSERT INTO clienteembalagementrada 
                                               (cdcliente, cdembalagem, vlentrada, vlsaida, dtentrada, 
                                                flsincronizado, txobs, txlatitude, txlongitude) 
                                                VALUES ("${cdcliente}", "${cdembalagem}", "${vlentrada}", 
                                                "${vlsaida}", "${dtentrada}", "${flsincronizado}", "${txobs}", 
                                                "${txLatitude}", "${txLongitude}")`, [],
                                        function (tx, res) {

                                        }, function (jsonError) {
//                                    console.log("***Error while select clienteembalagementrada e16: ");
//                                    logObject(jsonError);
//                                    console.log(jsonError);
//                                    console.log(JSON.stringify(jsonError));
                                });
                            }
                        }, function (jsonError) {
//                    console.log("*Error while select clienteembalagem :e14 ");
//                    logObject(jsonError);
                });
            }, function (jsonError) {
//        console.log("**Error while conecta table clienteembalagem :e15 ");
//        logObject(jsonError);
    }, function () {
//        console.log("conexao successfully");
    });
}

function atualizaClienteEmbalagemEntrada(cdcliente, cdembalagem, vlentrada, vlsaida, dtentrada, txobs) {
    new Date(dtentrada)
//    console.log(`UPDATE clienteembalagementrada SET
//                                vlentrada = "${vlentrada}", vlsaida = "${vlsaida}", dtentrada = "${dtentrada}", 
//                                txobs = "${txobs}", txlatitude = "${txLatitude}", txlongitude = "${txLongitude}"
//                              WHERE cdcliente = "${cdcliente}" AND cdembalagem = "${cdembalagem}" `)
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`UPDATE clienteembalagementrada SET
                                vlentrada = "${vlentrada}", vlsaida = "${vlsaida}", dtentrada = "${dtentrada}", 
                                txobs = "${txobs}", txlatitude = "${txLatitude}", txlongitude = "${txLongitude}"
                              WHERE cdcliente = "${cdcliente}" AND cdembalagem = "${cdembalagem}" `, [],
                        function (tx, ress) {
//                            console.log(`SELECT * FROM clienteembalagementrada
//                               WHERE cdcliente = "${cdcliente}" AND 
//                               cdembalagem = "${cdembalagem}" `)
                            tx.executeSql(`SELECT * FROM clienteembalagementrada
                               WHERE cdcliente = "${cdcliente}" AND 
                               cdembalagem = "${cdembalagem}" `, [], function (tx, res) {
//                                console.log('res.rows.length clienteembalagementrada')
//                                logObject(res.rows.item(0))
                            }, function (jsonError) {
                                //console.log("*Error while select logadoggggggg : ");
                                //logObject(jsonError);
                            });
                        }, function (jsonError) {
                    //console.log("*Error while select clienteembalagementrada589 : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while update table clienteembalagementrada : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}

function deletaClienteEmbalagemEntrada(cdcliente, cdembalagem) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`DELETE FROM clienteembalagementrada 
                              WHERE cdcliente = "${cdcliente}" AND cdembalagem = "${cdembalagem}" `);
            }, function (jsonError) {
        //console.log("Error while inserir table clienteembalagementrada : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}

function retornaClienteEmbalagemEntrada() {
    //console.log("pageName: " + pageName)
    if (pageName != 'lista-clientes' && pageName != 'entrega-caixa' && pageName != 'retorno-caixa' && pageName != 'iniciar-conferencia') {
        DBStorage.changeVersion(DBStorage.version, DBVersion,
                function (tx) {
                    tx.executeSql(`SELECT * FROM clienteembalagementrada WHERE vlentrada > 0 OR vlsaida > 0 `, [], function (tx, ress) {
                        //console.log('RRRress.rows.length')
                        //console.log(ress.rows.length)
                        if (ress.rows.length > 0) {
                            //abre botão sincronizar 
                            if (temInternet()) {
                                if (cdMotorista) {
                                    $(".btSincronizar").removeClass('text-color-gray border-color-gray').addClass('text-color-custom border-color-custom').attr('href', 'javascript:sincronizar();');
                                }
                                if (cdPromotor) {
                                    $(".btSincronizarP").removeClass('hide')
                                }
                            } else {
                                $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                                $(".btSincronizarP").addClass('hide')
                            }
                        } else {
                            retornaTransferenciaCaminhao()
                        }
                    }, function (jsonError) {
                        //console.log("*Error while select logado : ");
                        //logObject(jsonError);
                    });
                }, function (jsonError) {
            //console.log("Error while inserir table clienteembalagementrada : ");
            //logObject(jsonError);
        }, function () {
            ////console.log("DB Created successfully");
        });
    } else {
        $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
        $(".btSincronizarP").addClass('hide')
    }
}

function retornaTransferenciaCaminhao() {
    if (pageName != 'lista-clientes' && pageName != 'entrega-caixa' && pageName != 'retorno-caixa') {
        DBStorage.changeVersion(DBStorage.version, DBVersion,
                function (tx) {
                    tx.executeSql(`SELECT * FROM transferencia_caminhao `, [], function (tx, ress) {
                        if (ress.rows.length > 0) {
                            //abre botão sincronizar 
                            if (temInternet()) {
                                if (cdMotorista) {
                                    $(".btSincronizar").removeClass('text-color-gray border-color-gray').addClass('text-color-custom border-color-custom').attr('href', 'javascript:sincronizar();');
                                }
                            } else {

                                $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                                $(".btSincronizarP").addClass('hide')
                            }
                        } else {

                            $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                            $(".btSincronizarP").addClass('hide')
                        }
                    }, function (jsonError) {
                        //console.log("*Error while select logado : ");
                        //logObject(jsonError);
                    });
                }, function (jsonError) {
            //console.log("Error while inserir table clienteembalagementrada : ");
            //logObject(jsonError);
        }, function () {
            ////console.log("DB Created successfully");
        });
    } else {
        $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
        $(".btSincronizarP").addClass('hide')
    }
}

function retornaClienteEmbalagemPromotorSincronizar() {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT C.cdcliente, E.* FROM clienteembalagementrada E, cliente C
                              WHERE C.cdcliente = E.cdcliente AND vlentrada > 0 OR vlsaida > 0 
                              GROUP BY E.cdclienteembalagementrada  `, [], function (tx, ress) {
                    if (ress.rows.length > 0) {
                        for (x = 0; x < ress.rows.length; x++) {
                            arrSincronizar[x] = ress.rows.item(x);
                            enviarSincronizarEntradasEmbalagemPromotor(x, ress.rows.length);
                        }
                    } else {
                        retornaClientesSaldoAtualiza(0);
                        app.dialog.close();
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while inserir table clienteembalagementrada : ");
        //logObject(jsonError);
    }, function () {
        //  ////console.log("DB Created successfully");
    });
}

function retornaClienteEmbalagemEntradaSincronizar(tipo) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`SELECT C.cdcliente, E.* FROM clienteembalagementrada E, cliente C
                              WHERE C.cdcliente = E.cdcliente AND vlentrada > 0 OR vlsaida > 0 
                              GROUP BY E.cdclienteembalagementrada  `, [], function (tx, ress) {
                    if (ress.rows.length > 0) {
                        for (x = 0; x < ress.rows.length; x++) {
                            arrSincronizar[x] = ress.rows.item(x);
                            //logObject(ress.rows.item(x))
                            enviarSincronizarEntradasEmbalagem(x, ress.rows.length, tipo);
                        }
                    } else {
                        if (tipo == 1 || tipo == 3) {
                            sincronizaCaminhao(arrLiberacao['cdliberacao'], tipo);
                        } else {
                            sincronizaCaminhao(arrLiberacao['cdliberacao'], tipo);
                            //atualizaSituacaoLiberacao(arrLiberacao['cdliberacao'])
                        }
                    }
                }, function (jsonError) {
                    //console.log("*Error while select logado : ");
                    //logObject(jsonError);
                });
            }, function (jsonError) {
        //console.log("Error while inserir table clienteembalagementrada : ");
        //logObject(jsonError);
    }, function () {
        //  ////console.log("DB Created successfully");
    });
}

function atualizaSincronizadoClienteEmbalagem(cdclienteembalagem, flsincronizado) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`UPDATE clienteembalagem SET flsincronizado = "${flsincronizado}" 
                               WHERE cdclienteembalagem = "${cdclienteembalagem}" `);
            }, function (jsonError) {
        //console.log("Error while update table clienteembalagem : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}

function atualizaSincronizadoClienteEmbalagemEntrada(cdclienteembalagementrada, flsincronizado) {
    DBStorage.changeVersion(DBStorage.version, DBVersion,
            function (tx) {
                tx.executeSql(`UPDATE clienteembalagementrada SET flsincronizado = "${flsincronizado}" 
                               WHERE cdclienteembalagementrada = "${cdclienteembalagementrada}" `);
            }, function (jsonError) {
        //console.log("Error while update table clienteembalagementrada : ");
        //logObject(jsonError);
    }, function () {
        ////console.log("DB Created successfully");
    });
}
