function login() {
    if (temInternet()) {
        $('.page-current').find("#btLogar").addClass('hide');
        $('.page-current').find("#btLogando").removeClass('hide');
        if ($("#txusuario").val() == '' || $("#txsenha").val() == '') {
            $("#txusuario").addClass('input-error');
            $("#txsenha").addClass('input-error');
            abrirAvisos("preencha os campos", "background-color-overlay-red");
            $('.page-current').find("#btLogando").addClass('hide');
            $('.page-current').find("#btLogar").removeClass('hide');
            return;
        } else {
            $.ajax({
                type: "POST",
                crossDomain: true,
                dataType: 'json',
                url: "http://177.220.156.147/",
                data: {acao: 'retornaMotoristaLogin', txlogin: $("#txusuario").val(), txsenha: $("#txsenha").val(), uuid: uuid},
                success: function (data) {
                    logObject("***retornaMotoristaLogin***\n")
                    logObject(data)
                    if (!data.error) {
                        if (data.flmotorista) {
                            dtentrada = retornaHojeC();
                            //valida usuario e senha
                            cdMotorista = data.cdmotorista;
                            txMotorista = data.txmotorista;
                            insereMotorista(cdMotorista, txMotorista);

                            cdVeiculo = data.cdveiculo;
                            txVeiculo = data.txveiculo;

                            if (data.liberacao) {
                                arrLiberacao['cdliberacao'] = data.liberacao.cdliberacao;
                                cdliberacao = data.liberacao.cdliberacao;
                                txLiberacao = data.liberacao.cdliberacao;
                                arrLiberacao['embalagens'] = data.liberacao.embalagens;
                            }

                            if (txLiberacao) {
                                retornaHistoricoTransferenciasLiberacao();
                            }

                            if (data.clientes) {
                                //retorna Cliente
                                arrClientes = data.clientes;
                                if (arrClientes) {
                                    for (x in arrClientes) {
                                        insereCliente(arrClientes[x].cdcliente, arrClientes[x].txcliente, arrClientes[x].txrazaosocial, arrClientes[x].txcidade, arrClientes[x].txuf);
                                        if (arrClientes[x].embalagens) {
                                            for (y in arrClientes[x].embalagens) {
                                                insereClienteEmbalagem(arrClientes[x].embalagens[y].cdclienteembalagem, arrClientes[x].cdcliente, arrClientes[x].embalagens[y].cdembalagem, arrClientes[x].embalagens[y].txembalagem, arrClientes[x].embalagens[y].vlsaldo, 1, dtentrada);
                                            }
                                        }
                                    }
                                }
                            } else {
                                arrClientes = [];
                            }
                            //logObject(data.embalagens)
                            if (data.embalagens) {
                                arrEmbalagens = data.embalagens;
                                if (arrEmbalagens) {
                                    for (e in arrEmbalagens) {
                                        arrEmbalagens[e].vlcxvazia = Number(arrEmbalagens[e].vlcxvazia)
                                        insereEmbalagem(arrEmbalagens[e].cdembalagem, arrEmbalagens[e].txembalagem, arrEmbalagens[e].vlcxvazia)
                                    }
                                }
                            } else {
                                arrEmbalagens = [];
                            }

                            if (data.liberacoes) {
                                arrLiberacoes = data.liberacoes;
                                if (arrLiberacoes) {
                                    for (l in arrLiberacoes) {
                                        insereLiberacoes(arrLiberacoes[l].cdliberacao, arrLiberacoes[l].cdveiculo, arrLiberacoes[l].txveiculo, arrLiberacoes[l].cdmotorista, arrLiberacoes[l].txmotorista);
                                    }
                                }
                            } else {
                                arrLiberacoes = [];
                            }

                            $(".menusair").removeClass('hide')

                            flFazDirect = 1;
                            setInterval(function () {
                                if (flFazDirect == 1) {
                                    flFazDirect = 0;
                                    mainView.router.navigate('/carregar-dados', {
                                        reloadCurrent: true, //troca a pagina atual por essa remove do historico
                                        reloadPrevious: false, // troca a anterior por essa
                                        reloadAll: false // zera a rota e começa de novo, usar no logout
                                    })
                                }
                            }, 3000);

                        } else {

                            dtentrada = retornaHojeC();
                            //valida usuario e senha
                            cdPromotor = data.cdpromotor;
                            txPromotor = data.txpromotor;
                            inserePromotor(cdPromotor, txPromotor);


                            if (data.clientes) {
                                //retorna Cliente
                                arrClientes = data.clientes;
                                if (arrClientes) {
                                    for (x in arrClientes) {
                                        insereCliente(arrClientes[x].cdcliente, arrClientes[x].txcliente, arrClientes[x].txrazaosocial, arrClientes[x].txcidade, arrClientes[x].txuf);
                                        if (arrClientes[x].embalagens) {
                                            for (y in arrClientes[x].embalagens) {
                                                insereClienteEmbalagem(arrClientes[x].embalagens[y].cdclienteembalagem, arrClientes[x].cdcliente, arrClientes[x].embalagens[y].cdembalagem, arrClientes[x].embalagens[y].txembalagem, arrClientes[x].embalagens[y].vlsaldo, 1, dtentrada);
                                            }
                                        }
                                    }
                                }
                            }

                            if (data.embalagens) {
                                arrEmbalagens = data.embalagens;
                                if (arrEmbalagens) {
                                    for (e in arrEmbalagens) {
                                        arrEmbalagens[e].vlcxvazia = Number(arrEmbalagens[e].vlcxvazia)
                                        insereEmbalagem(arrEmbalagens[e].cdembalagem, arrEmbalagens[e].txembalagem, arrEmbalagens[e].vlcxvazia)
                                    }
                                }
                            }

                            $(".menusair").removeClass('hide')

                            flFazDirect = 1;
                            setInterval(function () {
                                if (flFazDirect == 1) {
                                    flFazDirect = 0;
                                    mainView.router.navigate('/lista-clientes', {
                                        reloadCurrent: true, //troca a pagina atual por essa remove do historico
                                        reloadPrevious: false, // troca a anterior por essa
                                        reloadAll: false // zera a rota e começa de novo, usar no logout
                                    })

                                }
                            }, 3000);
                        }
                        intervalSinc = setInterval(function () {
                            if (cdMotorista || cdPromotor) {
                                retornaClienteEmbalagemEntrada();
                            }
                        }, 1000);
                    } else {
                        $('.page-current').find("#btLogando").addClass('hide');
                        $('.page-current').find("#btLogar").removeClass('hide');
                        abrirAvisos(data.error, "background-color-overlay-red");
                        return;
                    }
                }, error: function (xhr) {
                    logObject(xhr)
                    $('.page-current').find("#btLogando").addClass('hide');
                    $('.page-current').find("#btLogar").removeClass('hide');
                    abrirAvisos("ocorreu um erro, tente mais tarde");
                    return;
                }
            })
        }
    } else {
        abreModalSemInternet();
    }
}

function carregarDados() {
    //Busca webservice Dados da liberação
    $.ajax({
        type: "POST",
        crossDomain: true,
        dataType: 'json',
        url: "http://177.220.156.147/",
        data: {acao: 'retornaLiberacaoMotorista', cdmotorista: cdMotorista},
        success: function (data) {
//            //logObject("***retornaLiberacao***\n")
//            //logObject(data)
            if (!data.error) {
                dtentrada = retornaHojeC();

                cdVeiculo = data.cdveiculo;
                txVeiculo = data.txveiculo;
                $("#txVeiculo").html(txVeiculo);

                if (data.liberacao) {
                    if (data.liberacao.cdliberacao) {
                        arrLiberacao['cdliberacao'] = data.liberacao.cdliberacao;
                        cdliberacao = data.liberacao.cdliberacao;
                        txLiberacao = data.liberacao.cdliberacao;
                        arrLiberacao['embalagens'] = data.liberacao.embalagens;

                        montaHmtlLiberacao(arrLiberacao);
                    } else {
                        abrirAvisos("Nenhuma liberação encontrada, tente mais tarde");
                        return;
                    }
                } else {
                    abrirAvisos("Nenhuma liberação encontrada, tente mais tarde");
                    return;
                }

                if (data.clientes) {
                    //retorna Cliente
                    arrClientes = data.clientes;
                    deleteTodosClientes(arrClientes);
                }

                if (data.embalagens) {
                    arrEmbalagens = data.embalagens;
                    deletaEmbalagem(arrEmbalagens);
                }

                if (data.liberacoes) {
                    arrLiberacoes = data.liberacoes;
                    deletaLiberacoes(arrLiberacoes);
                }

            }
        }, error: function (xhr) {
            abrirAvisos("ocorreu um erro, tente mais tarde");
            return;
        }
    })
}

function encerrarConferencia() {
    if (temInternet()) {
        app.dialog.create({
            title: 'Controle de Caixas',
            text: 'Tem Certeza que terminou a conferência?',
            buttons: [
                {
                    text: 'Não',
                    onClick: function () {
                        app.dialog.close();
                    }
                },
                {
                    text: 'Sim',
                    bold: true,
                    onClick: function () {
                        $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                        $(".btSincronizarP").addClass('hide')
                        app.dialog.preloader('Sincronizando...');
                        app.dialog.close();
                        contSincronizar = 0;
                        arrSincronizar = arrSincronizar.slice(1, 1);
                        clearInterval(intervalSinc);
                        clearInterval(intervalCoord);

                        retornaClienteEmbalagemPromotorSincronizar(2);
                    }
                },
            ],
        }).open();
    } else {
        app.dialog.close();
        abreModalSemInternet();
    }
}

function encerrarCarregamento() {
    if (temInternet()) {
        app.dialog.create({
            title: 'Controle de Caixas',
            text: 'Tem Certeza que terminou o carregamento?',
            buttons: [
                {
                    text: 'Não',
                    onClick: function () {
                        app.dialog.close();
                    }
                },
                {
                    text: 'Sim',
                    bold: true,
                    onClick: function () {
                        $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                        $(".btSincronizarP").addClass('hide')
                        app.dialog.preloader('Sincronizando...');
                        app.dialog.close();
                        contSincronizar = 0;
                        arrSincronizar = arrSincronizar.slice(1, 1);
                        clearInterval(intervalSinc);
                        clearInterval(intervalCoord);
                        retornaClienteEmbalagemEntradaSincronizar(2);
                    }
                },
            ],
        }).open();
    } else {
        app.dialog.close();
        abreModalSemInternet();
    }
}

function sincronizarPromotor() {
    if (temInternet()) {
        app.dialog.create({
            title: 'Controle de Caixas',
            text: 'Tem Certeza que deseja sincronizar?',
            buttons: [
                {
                    text: 'Não',
                    onClick: function () {
                        app.dialog.close();
                    }
                },
                {
                    text: 'Sim',
                    bold: true,
                    onClick: function () {
                        $(".btSincronizarP").addClass('hide')
                        app.dialog.preloader('Sincronizando...');
                        app.dialog.close();
                        contSincronizar = 0;
                        arrSincronizar = arrSincronizar.slice(1, 1);
                        clearInterval(intervalSinc);
                        clearInterval(intervalCoord);
                        retornaClienteEmbalagemPromotorSincronizar(2);
                    }
                },
            ],
        }).open();
    } else {
        app.dialog.close();
        abreModalSemInternet();
    }
}

function sincronizar() {
    app.dialog.create({
        title: 'Controle de Caixas',
        text: 'Tem Certeza que deseja sincronizar?',
        buttons: [
            {
                text: 'Não',
                onClick: function () {
                    app.dialog.close();
                }
            },
            {
                text: 'Sim',
                bold: true,
                onClick: function () {
                    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                    $(".btSincronizarP").addClass('hide')
                    app.dialog.preloader('Sincronizando...');
                    contSincronizar = 0;
                    arrSincronizar = arrSincronizar.slice(1, 1);
                    clearInterval(intervalSinc)
                    clearInterval(intervalCoord);
                    retornaClienteEmbalagemEntradaSincronizar(1);
                }
            },
        ],
    }).open();
}

function enviarSincronizarEntradasEmbalagemPromotor(x, tamX, tipo) {
    $(".btSincronizarP").addClass('hide')
    // //logObject("EntreienviarSincronizarEntradasEmbalagemPromotor")
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: "http://177.220.156.147/",
        data: {acao: 'enviarSincronizarEntradasEmbalagemPromotor', cdcliente: arrSincronizar[x].cdcliente, cdfiliais: 1, cdpromotor: cdPromotor, dtentrada: arrSincronizar[x].dtentrada, cdembalagem: arrSincronizar[x].cdembalagem, vlentrada: arrSincronizar[x].vlentrada, vlsaida: arrSincronizar[x].vlsaida, txobs: arrSincronizar[x].txobs},
        success: function (data) {
            //   //logObject("enviarSincronizarEntradasEmbalagemPromotor")
            //logObject("aki1")
            $(".btSincronizarP").addClass('hide')
            dtentrada = retornaHojeC()
            atualizaClienteEmbalagemEntrada(arrSincronizar[x].cdcliente, arrSincronizar[x].cdembalagem, 0, 0, dtentrada);

            if (arrClientes) {
                //logObject("aki1B")
                for (c in arrClientes) {
                    if (arrSincronizar[x].cdcliente == arrClientes[c].cdcliente) {
                        if (arrClientes[c].embalagens) {
                            for (e in arrClientes[c].embalagens) {
                                //logObject("before")
                                //logObject(arrClientes[c].embalagens[e])
                                arrClientes[c].embalagens[e].vlsaida = 0;
                                arrClientes[c].embalagens[e].vlentrada = 0;
                                arrClientes[c].embalagens[e].txobs = "";
                                //logObject(arrClientes[c].embalagens[e])
                                //console.log("after")
                            }
                        }
                        //logObject(arrClientes[c].embalagens)
                    }
                }
            }
            contSincronizar++;
            //logObject("aki12")
            //logObject(tamX)
            //logObject(contSincronizar)
            //logObject("aki1333")
            if (tamX == contSincronizar) {
                //logObject("aki1N")
                //logObject("aki13")
                //logObject(arrClientes)
                $(".btSincronizarP").addClass('hide')
                // zerarBancodeDadosDB();  
                //logObject("aki14")
                retornaClientesSaldoAtualiza(0);
            }
        }, error: function (xhr) {
            abrirAvisos("ocorreu um erro, tente mais tarde");
            return;
        }
    })
}

function enviarSincronizarEntradasEmbalagem(x, tamX, tipo) {
    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
    //console.log("\n\ncdclienteembalagementrada\n")
    //console.log(arrSincronizar[x].cdclienteembalagementrada)
    logObject("Inicio enviarSincronizarEntradasEmbalagem:");
    logObject(arrSincronizar[x]);
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: "http://177.220.156.147/",
        data: {acao: 'enviarSincronizarEntradasEmbalagem', cdliberacao: arrLiberacao['cdliberacao'], cdcliente: arrSincronizar[x].cdcliente, cdfiliais: 1, cdmotorista: cdMotorista, dtentrada: arrSincronizar[x].dtentrada, cdembalagem: arrSincronizar[x].cdembalagem, vlentrada: arrSincronizar[x].vlentrada, vlsaida: arrSincronizar[x].vlsaida, cdclienteembalagementrada: arrSincronizar[x].cdclienteembalagementrada, txobs: arrSincronizar[x].txobs, txlatitude: arrSincronizar[x].txlatitude, txlongitude: arrSincronizar[x].txlongitude},
        success: function (data) {
            logObject('enviarSincronizarEntradasEmbalagem retorno ajax')
            logObject(data)
            $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
            dtentrada = retornaHojeC()
            atualizaClienteEmbalagemEntrada(arrSincronizar[x].cdcliente, arrSincronizar[x].cdembalagem, 0, 0, dtentrada);

            if (arrClientes) {
                for (c in arrClientes) {
                    if (arrClientes[c].cdcliente == arrSincronizar[x].cdcliente) {
                        if (arrClientes[c].embalagens) {
                            for (y in arrClientes[c].embalagens) {
                                if (arrClientes[c].embalagens[y].cdembalagem == arrSincronizar[x].cdembalagem) {
                                    arrClientes[c].embalagens[y].vlentrada = 0;
                                    arrClientes[c].embalagens[y].vlsaida = 0;
                                }
                            }
                        }
                    }
                }
            }
            contSincronizar++;
            if (tamX == contSincronizar) {
                if (tipo == 1 || tipo == 3) {
                    sincronizaCaminhao(arrLiberacao['cdliberacao'], tipo);
                    //retornaClientesSaldoAtualiza();
                } else {
                    sincronizaCaminhao(arrLiberacao['cdliberacao'], tipo);
                    //atualizaSituacaoLiberacao(arrLiberacao['cdliberacao'])
                }
            }
        }, error: function (xhr) {
            abrirAvisos("ocorreu um erro, tente mais tarde");
            return;
        }
    })
}

function sincronizaCaminhao(cdliberacao, tipo) {
    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
    contSincT = 0;
    arrSincronizarTCaminhoes = arrSincronizarTCaminhoes.slice(1, 1);
    retornaCaminhao(cdliberacao, tipo);
}

function inseretCaminhoesTransferencia(cdliberacao, t, tamT, tipo) {
    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')

    //  //console.log('rrSincronizarTCaminhoes')
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: "http://177.220.156.147/",
        data: {acao: 'inseretCaminhoesTransferencia', cdfiliais: 1, cdmotoristaOrigem: cdMotorista, cdmotoristaDestino: arrSincronizarTCaminhoes[t].cdmotorista, cdliberacaoOrigem: cdliberacao, cdliberacaoDestino: arrSincronizarTCaminhoes[t].cdliberacao, cdproduto: arrSincronizarTCaminhoes[t].cdembalagem, dttranferencia: arrSincronizarTCaminhoes[t].dttransferir, vlquantidade: arrSincronizarTCaminhoes[t].vlquantidade},
        success: function (data) {
            $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
            contSincT++;
            deletaCaminhaoTransferencia(arrSincronizarTCaminhoes[t].cdmotorista, arrSincronizarTCaminhoes[t].cdliberacao, arrSincronizarTCaminhoes[t].cdembalagem, arrSincronizarTCaminhoes[t].dttransferir, arrSincronizarTCaminhoes[t].vlquantidade);

            if (contSincT == tamT) {
                if (tipo == 1 || tipo == 3) {
                    //baixar tranferencia para gerar historico
                    retornaClientesSaldoAtualiza(tipo);
                } else {
                    atualizaSituacaoLiberacao(cdliberacao);
                }
            }
        }
        , error: function (xhr) {
            logObject(xhr)
            abrirAvisos("ocorreu um erro, tente mais tarde");
            return;
        }
    })
}

function atualizaSituacaoLiberacao(cdliberacao) {
    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
//    //logObject("data")
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: "http://177.220.156.147/",
        dataType: 'json',
        data: {acao: 'atualizaSituacaoLiberacao', cdliberacao: cdliberacao},
        success: function (data) {
            $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
//            //logObject(data)
            if (data.embalagens) {
                htmlFinalizaLiberacao = `<div class="list media-list inset ml0 mr0">
            <ul>
                <li>
                    <div class="item-link item-content">
                        <div class="item-inner pt0">
                            <div class="item-subtitle fLeft">Nº Liberação:</div>
                            <div class="item-text fLeft ml16 mt2 fontweightbold" > ${cdliberacao} </div>
                            <div class="item-text font11 clear">*As quantidades foram atualizadas com as entregas, transferências e retornos</div>
                        </div> 
                    </div>
                </li>
            </ul>`;
                for (x in data.embalagens) {
                    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                    htmlFinalizaLiberacao += `
            <ul class="background-gray ml16 mt16 mr16">
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600">${data.embalagens[x].txembalagem}</div>
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pt12 pb12 wautoFL">Total carregadas</div>
                        <div class="item-input-wrap  w100FR ml16">
                             <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.embalagens[x].qtdCarregamento)}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pt12 pb12 wautoFL">Total entregues</div>
                        <div class="item-input-wrap  w100FR ml16">
                             <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.embalagens[x].qtdEntrega)}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pt12 pb12 wautoFL">Total retornadas</div>
                        <div class="item-input-wrap  w100FR ml16">
                             <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.embalagens[x].qtdRetorno)}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pt12 pb12 wautoFL">Total Transferidas (Saída)</div>
                        <div class="item-input-wrap  w100FR ml16">
                             <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.embalagens[x].qtdEnviado)}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pt12 pb12 wautoFL">Total Transferidas (Entrada)</div>
                        <div class="item-input-wrap  w100FR ml16">
                             <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.embalagens[x].qtdRecebido)}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input border-top-dotted pt16">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pt12 pb12 wautoFL">Saldo</div>
                        <div class="item-input-wrap  w100FR ml16">
                             <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.embalagens[x].vlSaldo)}">
                        </div>
                    </div>
                </li>
            </ul>`;
                }
                htmlFinalizaLiberacao += `
            <ul class="background-gray ml16 mt16 mr16">
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600 wautoFL">Total de Embalagens</div>
                            <div class="item-input-wrap  w100FR ml16">
                                 <input class="background-input-gray" type="number" step="1" readonly value="${parseInt(data.vlSaldo)}">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="block">
                <p class="row">
                    <a href="javascript:zerarBancodeDadosDB('${cdMotorista}','${txMotorista}');" class="col button button-big button-fill background-color-custom">fechar</a>
                </p>
            </div>
        </div>`;
                $('.page-current').find(".page-content").html(htmlFinalizaLiberacao);
                $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                app.dialog.close();
            } else {
                zerarBancodeDadosDB(cdMotorista, txMotorista);
                app.dialog.close();
            }

//           
//           
        }
        , error: function (xhr) {
            logObject(xhr)
            abrirAvisos("ocorreu um erro, tente mais tarde");
            return;
        }
    })
}

function retornaClientesSaldoAtualiza(tipo) {
    //logObject("aki15")
    $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
    $(".btSincronizarP").addClass('hide')
    // //console.log('retornaClientesSaldoAtualiza')
    if (temInternet()) {
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: 'json',
            url: "http://177.220.156.147/",
            data: {acao: 'retornaClientescomSaldo'},
            success: function (data) {
                $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
                $(".btSincronizarP").addClass('hide')
                //logObject("aki16")
                //logObject("retornaClientescomSaldo\n")
                //logObject(data)
                if (!data.error) {
                    if (tipo == 0 || !tipo) {
                        for (x in data) {
                            retornaInsereClientes(data[x], x, data.length)
                        }
                    } else {
                        //retorna Veiculos
                        arrClientes = data;
                        app.dialog.close();
                        deleteTodosClientes(arrClientes);
                        if (tipo == 3) {
                            voltarPagina();
                        }
                    }
                } else {
                    app.dialog.close();
                }
                intervalSinc = setInterval(function () {
                    if (cdMotorista || cdPromotor) {
                        retornaClienteEmbalagemEntrada();
                    }
                }, 1000);
            }
            , error: function (xhr) {
                logObject(xhr)
                abrirAvisos("ocorreu um erro, tente mais tarde");
                return;
            }
        })
    } else {
        app.dialog.close();
        abreModalSemInternet();
    }
}

function retornaTodosMotoristasVeiculoLiberacao() {
    if (temInternet()) {
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: 'json',
            url: "http://177.220.156.147/",
            data: {acao: 'retornaTodosMotoristasVeiculoLiberacao', cdliberacao: txLiberacao},
            success: function (data) {
//                //logObject("retornaTodosMotoristasVeiculoLiberacao")
//                //logObject(data)

                if (!data.error) {
                    //retorna Veiculos
                    arrLiberacoes = data;
                    deletaLiberacoes(arrLiberacoes);
                    montaHTMLTransferirCaminhao(arrLiberacoes);
                }
            }
            , error: function (xhr) {
                logObject(xhr)
                abrirAvisos("ocorreu um erro, tente mais tarde");
                return;
            }
        })
    } else {
        //traz arrLiberacoes local se não tem internet
        montaHTMLTransferirCaminhao(arrLiberacoes);
    }
}

function sair() {
    app.dialog.create({
        title: 'Controle de Caixas',
        text: 'Tem Certeza que deseja sair?',
        buttons: [
            {
                text: 'Não',
                onClick: function () {
                    app.dialog.close();
                }
            },
            {
                text: 'Sim',
                bold: true,
                onClick: function () {
                    zerarBancodeDadosDB();
                    app.dialog.close();
                }
            },
        ],
    }).open();
}

function abrirBuscarOutros() {
    modalBusca = 1;
    app.dialog.create({
        title: 'Buscar Outros Clientes',
        content: `<div class="dialog-input-field item-input">
                        <div class="item-input-wrap">
                            <input class="dialog-input" type="text" required  id="txbuscacliente" >
                        </div>
                    </div>`,
        buttons: [
            {
                text: 'fechar',
                onClick: function () {
                    modalBusca = 0;
                    app.dialog.close();
                }
            },
            {
                text: 'Buscar',
                bold: true,
                onClick: function () {
                    if ($("#txbuscacliente").val() == '') {
                        abrirAvisos("Preencha o campo de busca", "background-color-overlay-red");
                    } else {
                        app.dialog.preloader('Buscando...');
                        app.dialog.close();
                        buscarOutros($("#txbuscacliente").val());
                    }
                }
            }
        ],
    }).open();
}

function buscarOutros(txbusca) {
    if (temInternet()) {
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: 'json',
            url: "http://177.220.156.147/",
            data: {acao: 'retornaBuscaClientes', txbusca: txbusca},
            success: function (data) {
                //logObject('retornaBuscaClientes')
                //logObject(data)
                if (!data.error) {
                    for (x in data) {
                        retornaInsereClientes(data[x], x, data.length, txbusca)
                    }
                } else {
                    app.dialog.close();
                    modalBusca = 0;
                    abrirAvisos("nenhum resultado encontradao", "", 1);
                }
            }, error: function (xhr) {
                logObject(xhr)
                abrirAvisos("ocorreu um erro, tente mais tarde");
                return;
            }
        })
    } else {
        app.dialog.close();
        modalBusca = 0;
        abreModalSemInternet();
    }
}

function retornaHistoricoTransferenciasLiberacao() {
    if (temInternet()) {
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: 'json',
            url: "http://177.220.156.147/",
            data: {acao: 'retornaTodasTranfereciaCaminhaoLiberacao', cdliberacao: txLiberacao},
            success: function (data) {
                //logObject("retornaTodasTranfereciaCaminhaoLiberacao")
                //logObject(data)
                if (!data.error) {
                    if (data.enviados) {
                        arrEnviados = data.enviados;
                        for (x in data.enviados) {
                            deletaTransferenciaHistorico(data.enviados[x])
                        }
                    }
                    if (data.recebidos) {
                        arrRecebidos = data.recebidos;
                        for (x in data.recebidos) {
                            deletaTransferenciaHistorico(data.recebidos[x])
                        }
                    }
                }
            }, error: function (xhr) {
                logObject(xhr)
                return;
            }
        })
    }
}

function listaHistoricoTransferenciasLiberacao(fltipo) {
    //console.log("kakaka" + txLiberacao)
    if (temInternet()) {
        $.ajax({
            type: "POST",
            crossDomain: true,
            dataType: 'json',
            url: "http://177.220.156.147/",
            data: {acao: 'retornaTodasTranfereciaCaminhaoLiberacao', cdliberacao: txLiberacao},
            success: function (data) {
                //logObject("retornaTodasTranfereciaCaminhaoLiberacao")
                //logObject(data)
                if (!data.error && (data.enviados || data.recebidos)) {
                    if (data.enviados) {
                        arrEnviados = data.enviados;
                        deletaTransferenciaHistorico(data.enviados)
                    }
                    if (data.recebidos) {
                        arrRecebidos = data.recebidos;
                        deletaTransferenciaHistorico(data.recebidos)
                    }
                    if (fltipo == 1) {
                        montaHtmlTranferencia(data.enviados)
                    } else {
                        montaHtmlTranferencia(data.recebidos)
                    }
                } else {
                    retornaTransferenciaHistoricoTipo(fltipo)
                }
            }, error: function (xhr) {
                retornaTransferenciaHistoricoTipo(fltipo)
                logObject(xhr)
                return;
            }
        })
    } else {
        retornaTransferenciaHistoricoTipo(fltipo)
    }
}

function transferirCaminhao() {
    if (temInternet()) {
        dttransferir = retornaHojeC();
        $('.page-current').find("#transferirCaminhao").addClass('hide');
        $('.page-current').find("#transferindoCaminhao").removeClass('hide');

        if ($("#cdliberacao_transferencia").val() == "") {
            $("#cdliberacao_transferencia").addClass('input-error');
            abrirAvisos("selecione o veiculo", "background-color-overlay-red");
            $('.page-current').find("#transferirCaminhao").removeClass('hide');
            $('.page-current').find("#transferindoCaminhao").addClass('hide');
            return;
        } else if ($("#txsenhatranferencia").val() == "") {
            $("#txsenhatranferencia").addClass('input-error');
            abrirAvisos("preencha a senha", "background-color-overlay-red");
            $('.page-current').find("#transferirCaminhao").removeClass('hide');
            $('.page-current').find("#transferindoCaminhao").addClass('hide');
            return;
        } else {
            cdmotorista = 0;
            if (arrLiberacoes) {
                for (x in arrLiberacoes) {
                    if (arrLiberacoes[x].cdliberacao == $("#cdliberacao_transferencia").val()) {
                        cdmotorista = arrLiberacoes[x].cdmotorista;
                        txmotorista = arrLiberacoes[x].txmotorista;
                        txveiculo = arrLiberacoes[x].txveiculo;
                    }
                }
            }
            $.ajax({
                type: "POST",
                crossDomain: true,
                url: "http://177.220.156.147/",
                data: {acao: 'validaMotoristaSenha', cdmotorista: cdmotorista, txsenha: $("#txsenhatranferencia").val()},
                success: function (data) {
                    //logObject("validaMotoristaSenha")
                    //logObject(data)
                    if ($.trim(data) == 'ok') {
                        if (arrEmbalagens) {
                            tam = 0;
                            for (x in arrEmbalagens) {
                                if ($(`#vltransferir${arrEmbalagens[x].cdembalagem}`).val() > 0) {
                                    tam++;
                                }
                            }
							if (tam == 0) { 
								abrirAvisos("preencha a quantidade", "background-color-overlay-red");
								$('.page-current').find("#transferirCaminhao").removeClass('hide');
								$('.page-current').find("#transferindoCaminhao").addClass('hide');
								return;
							}
                            for (x in arrEmbalagens) {
                                if ($(`#vltransferir${arrEmbalagens[x].cdembalagem}`).val() > 0) {
                                    insereTransferenciaHistorico(dttransferir, arrEmbalagens[x].cdembalagem, arrEmbalagens[x].txembalagem, $(`#vltransferir${arrEmbalagens[x].cdembalagem}`).val(), txmotorista, txveiculo, 1, cdliberacao);
                                    $.ajax({
                                        type: "POST",
                                        crossDomain: true,
                                        url: "http://177.220.156.147/",
                                        data: {acao: 'inseretCaminhoesTransferencia', cdfiliais: 1, cdmotoristaOrigem: cdMotorista, cdmotoristaDestino: cdmotorista, cdliberacaoOrigem: cdliberacao, cdliberacaoDestino: $("#cdliberacao_transferencia").val(), cdproduto: arrEmbalagens[x].cdembalagem, dttranferencia: dttransferir, vlquantidade: $(`#vltransferir${arrEmbalagens[x].cdembalagem}`).val()},
                                        success: function (data) {
                                            tam--;
                                            if (tam == 0) {
                                                abrirAvisos("Transferencia efetuada com sucesso", "background-color-overlay-green");
                                                vlVoltarAgora = 1;
                                                setInterval(function () {
                                                    if (vlVoltarAgora == 1) {
                                                        vlVoltarAgora = 0;
                                                        voltarPagina();
                                                        app.toast.close();
                                                    }
                                                }, 1000);
                                            }
                                        }
                                        , error: function (xhr) {
                                            logObject(xhr)
                                            abrirAvisos("ocorreu um erro, tente mais tarde");
                                            return;
                                        }
                                    })
                                }
                            }
                        }
                    } else {
                        $("#txsenhatranferencia").addClass('input-error');
                        abrirAvisos("senha incorreta", "background-color-overlay-red");
                        $('.page-current').find("#transferirCaminhao").removeClass('hide');
                        $('.page-current').find("#transferindoCaminhao").addClass('hide');
                    }
                }
                , error: function (xhr) {
                    logObject(xhr)
                    abrirAvisos("ocorreu um erro, tente mais tarde");
                    return;
                }
            })
        }
    } else {
        abreModalSemInternet();
    }
}



