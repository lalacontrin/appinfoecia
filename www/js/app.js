// If we need to use custom DOM library, let's save it to $$ variable:
//http://187.18.106.240:8087/ - base teste
//177.220.156.147 - caseiriho
var $$ = Dom7;

var DBVersion = "2";
var DBStorage = window.openDatabase("DBcontroleCaixa.db", "", "Databse Controle De Caixas Amplus", 200000);

var cdMotorista = 0;
var txMotorista = '';
var cdPromotor = 0;
var txPromotor = '';
var cdVeiculo = 0;
var txVeiculo = '';
var txLiberacao = '';
var arrLiberacao = [];
var arrClientes = [];
var arrEmbalagens = [];
var arrRecebidos = [];
var arrEnviados = [];
var arrSincronizar = [];
var arrSincronizarTCaminhoes = [];
var arrLiberacoes = [];
var d_veiculos = [];

var intervalSinc;
var intervalCoord;

var contSincronizar = 0;
var contSincT = 0;
var pgInit = 0;
var pageName = '';

var txLatitude = "";
var txLongitude = "";

var modalBusca = 0;

var vlInputOld = '';

// Framework7 App main instance
var app = new Framework7({
    root: '#app',
    theme: 'auto', // Automatic theme detection 
    routes: routes,
    on: {
        // each object key means same name event handler
        pageInit: function (page) {
            console.log('page init:' + page.name);
            pgInit = 1;
        },
        pageBeforeIn: function (page) {
            console.log('page before in:' + page.name);
        },
        pageAfterIn: function (page) {
            console.log('page after in: ' + page.name);
            pageName = page.name;
            if (pgInit === 1) {
                pgInit = 0;
                if (page.name == 'carregar-dados') {
                    $("#txMotorista").html(txMotorista);
                    if (txVeiculo) {
                        $("#txVeiculo").html(txVeiculo);
                    }
                    if (arrLiberacao.cdliberacao) {
                        montaHmtlLiberacao(arrLiberacao);
                    } else {
                        $("#buscarLiberação").removeClass('hide')
                    }
                }

                if (page.name == 'inicial') {
                    montaTopoLiberacao();
                }

                if (page.name == 'lista-clientes') {
                    if (page.route.params.txbusca) {
                        $(".searchbarInput").val(page.route.params.txbusca)
                    }
                    htmlListClientes = `<div class="list searchbar-found media-list inset chevron-center mt0 ml0 mr0">
                                        <ul>`;
                    if (arrClientes) {
                        for (x in arrClientes) {
                            htmlListClientes += `<li>
                                                <a href="/cliente/${arrClientes[x].cdcliente}" class="item-link item-content">
                                                    <div class="item-inner">
                                                        <div class="item-title-row">
                                                            <div class="item-title item-search-result fontweight600">${arrClientes[x].txcliente}</div>
                                                        </div>
                                                        <div class="item-subtitle item-search-result">${arrClientes[x].txrazaosocial}</div>
                                                        <div class="item-subtitle">${arrClientes[x].txcidade} - ${arrClientes[x].txuf}</div>
                                                    </div>
                                                </a>
                                            </li>`;
                        }
                    }
                    htmlListClientes += `</ul>
                                        </div>
                                        <!-- Nothing found message -->
                                        <div class="block searchbar-not-found ml0 mr0">
                                            <div class="block-inner">Nenhum cliente encontrado</div>
                                        </div>`;
                    $(".page-current #recipListClientes").html(htmlListClientes);
                    var searchbar = app.searchbar.create({
                        el: '.searchbar',
                        searchContainer: '.searchbar-found',
                        searchIn: '.item-search-result',
                        on: {
                            search(sb, query, previousQuery) {
                                console.log(query, previousQuery);
                            }
                        }
                    }).search($(".searchbarInput").val());
                }

                if (page.name == 'cliente') {
                    flHTML = 0;
                    montaTopoLiberacao();
                    htmlClienteIni = '';
                    if (arrClientes) {
                        for (x in arrClientes) {
                            if (arrClientes[x].cdcliente == page.route.params.cdcliente) {
                                htmlClienteIni = `
                                                <li id="topoCliente">
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <div class="item-title-row">
                                                                <div class="item-title item-search-result fontweight600">${arrClientes[x].txcliente}</div>
                                                            </div>
                                                            <div class="item-subtitle">${arrClientes[x].txrazaosocial}</div>
                                                            <div class="item-subtitle">${arrClientes[x].txcidade} - ${arrClientes[x].txuf}</div>
                                                        </div>
                                                    </div>
                                                </li>`;

                                if (cdMotorista) {
                                    htmlClienteIni += ` 
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="/entrega-caixa/${arrClientes[x].cdcliente}" class="col button color-green button-big button-fill">Entrega de Caixas</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="/retorno-caixa/${arrClientes[x].cdcliente}" class="col button color-red button-big button-fill ">Retorno de Caixas</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="/consulta-saldo/${arrClientes[x].cdcliente}" class="col button color-gray button-big button-fill ">Consulta Saldo</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="javascript:encerrarMovimentacao();" class="col button button-outline button-big text-color-custom border-color-custom">Encerrar Movimentação</a>
                                                        </div>
                                                    </div>
                                                </li>`;
                                } else {
                                    htmlClienteIni += `
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="/iniciar-conferencia/${arrClientes[x].cdcliente}" class="col button background-color-custom button-big button-fill">Iniciar Conferência</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="/consulta-saldo-promotor/${arrClientes[x].cdcliente}" class="col button color-gray button-big button-fill ">Consulta Saldo</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item-content">
                                                        <div class="item-inner">
                                                            <a href="javascript:voltarPagina();" class="col button button-outline button-big text-color-custom border-color-custom">Alterar Cliente</a>
                                                        </div>
                                                    </div>
                                                </li> `;
                                }
                                if (!flHTML) {
                                    flHTML = 1;
                                    $("#recipClienteIni").prepend(htmlClienteIni);
                                    $(".loadRemove").remove();
                                }
                            }
                        }
                    }

                }

                if (page.name == 'entrega-caixa') {
                    flHTML = 0;
                    $("#recipClienteEntrega").html($(".page-previous").find('#topoCliente').html());
                    if (arrClientes) {
                        for (x in arrClientes) {
                            if (arrClientes[x].cdcliente == page.route.params.cdcliente) {
                                htmlClienteEntrega = ``;
                                /*usa o embalagem liberacao pois só pode entregar o que está no caminhão
                                 * no retorno usa o do cliente, pois só pode pegar embalagem que o cliente tenha*/
                                if (arrEmbalagens) {
                                    for (y in arrEmbalagens) {
                                        vlEntrada = "";
                                        txobs = "";
                                        if (arrClientes[x].embalagens) {
                                            for (z in arrClientes[x].embalagens) {
                                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                                    vlEntrada = arrClientes[x].embalagens[z].vlentrada;
                                                    if (arrClientes[x].embalagens[z].txobs && arrClientes[x].embalagens[z].txobs != 'undefined') {
                                                        txobs = arrClientes[x].embalagens[z].txobs;
                                                    }
                                                }
                                            }
                                        }
                                        htmlClienteEntrega += `<ul class="background-gray ml16  mr16">
                <li class="item-content item-input li-header-green">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600">${arrEmbalagens[y].txembalagem}</div>
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title item-label pt12 pb12 wautoFL">Quantidade Entregue</div>
                        <div class="item-input-wrap w100FL bolder-all ml16">
                            <input class="background-input-gray fontweightbold pl16" id="vlentrada${arrEmbalagens[y].cdembalagem}" name="vlentrada${arrEmbalagens[y].cdembalagem}" type="number"  step="1" value="${vlEntrada}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title item-label pb16">Observações</div>
                        <div class="item-input-wrap">
                            <textarea  id="txobs${arrEmbalagens[y].cdembalagem}" name="txobs${arrEmbalagens[y].cdembalagem}"  class="background-input-gray" >${txobs}</textarea>
                        </div>
                    </div>
                </li>
            </ul>`;
                                    }
                                }
                                htmlClienteEntrega += ` <div class="block">
                                                            <p class="row">
                                                                <a id="confirmarEntrega" href="javascript:confirmarEntrega(${arrClientes[x].cdcliente});" class="col button button-big button-fill background-color-custom">Confirmar</a>
                                                            </p> 
                                                            <p class="row">
                                                                <a id="confirmandoEntrega" class="col button button-big button-fill background-color-custom hide">
                                                                    <span class="preloader">
                                                                        <span class="preloader-inner">
                                                                            <span class="preloader-inner-gap"></span>
                                                                            <span class="preloader-inner-left">
                                                                                <span class="preloader-inner-half-circle"></span>
                                                                            </span>
                                                                            <span class="preloader-inner-right">
                                                                                <span class="preloader-inner-half-circle"></span>
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </p>
                                                        </div>`;
                                if (!flHTML) {
                                    flHTML = 1;
                                    $("#recipClienteEntrega").after(htmlClienteEntrega);
                                    $(".loadRemove").remove();
                                }

                            }
                        }
                    }
                }

                if (page.name == 'retorno-caixa') {
                    flHTML = 0;
                    $("#recipClienteRetorno").html($(".page-previous").find('#topoCliente').html());
                    if (arrClientes) {
                        for (x in arrClientes) {
                            if (arrClientes[x].cdcliente == page.route.params.cdcliente) {
                                htmlClienteRetorno = ``;
                                /*usa o embalagem liberacao pois só pode entregar o que está no caminhão
                                 * no retorno usa o do cliente, pois só pode pegar embalagem que o cliente tenha*/
                                if (arrEmbalagens) {
                                    for (y in arrEmbalagens) {
                                        vlSaida = "";
                                        txobs = "";
                                        if (arrClientes[x].embalagens) {
                                            for (z in arrClientes[x].embalagens) {
                                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                                    vlSaida = arrClientes[x].embalagens[z].vlsaida;
                                                    if (arrClientes[x].embalagens[z].txobs && arrClientes[x].embalagens[z].txobs != 'undefined') {
                                                        txobs = arrClientes[x].embalagens[z].txobs;
                                                    }
                                                }
                                            }
                                        }

                                        htmlClienteRetorno += `<ul class="background-gray mt16 ml16  mr16">
                <li class="item-content item-input li-header-red">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600">${arrEmbalagens[y].txembalagem}</div>
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title item-label pt12 pb12 wautoFL">Quantidade Retorno</div>
                        <div class="item-input-wrap w100FL bolder-all ml16">
                            <input class="background-input-gray fontweightbold pl16" type="number" step="1"  id="vlsaida${arrEmbalagens[y].cdembalagem}" name="vlsaida${arrEmbalagens[y].cdembalagem}"  value="${vlSaida}">
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title item-label pb16">Observações</div>
                        <div class="item-input-wrap">
                            <textarea  id="txobs${arrEmbalagens[y].cdembalagem}" name="txobs${arrEmbalagens[y].cdembalagem}"  class="background-input-gray"  >${txobs}</textarea>
                        </div>
                    </div>
                </li>
            </ul>`;
                                    }
                                }
                                htmlClienteRetorno += `<div class="block">
                                                            <p class="row">
                                                                <a id="confirmaRetorno" href="javascript:confirmaRetorno(${arrClientes[x].cdcliente});" class="col button button-big button-fill background-color-custom">Confirmar</a>
                                                            </p>
                                                            <p class="row">
                                                                <a id="confirmandoRetorno" class="col button button-big button-fill background-color-custom hide">
                                                                    <span class="preloader">
                                                                        <span class="preloader-inner">
                                                                            <span class="preloader-inner-gap"></span>
                                                                            <span class="preloader-inner-left">
                                                                                <span class="preloader-inner-half-circle"></span>
                                                                            </span>
                                                                            <span class="preloader-inner-right">
                                                                                <span class="preloader-inner-half-circle"></span>
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </p>
            </div>`;
                                if (!flHTML) {
                                    flHTML = 1;
                                    $("#recipClienteRetorno").after(htmlClienteRetorno);
                                    $(".loadRemove").remove();
                                }
                            }
                        }
                    }
                }

                if (page.name == 'iniciar-conferencia') {
                    flHTML = 0;
                    $("#recipConferencia").html($(".page-previous").find('#topoCliente').html());
                    if (arrClientes) {
                        for (x in arrClientes) {
                            if (arrClientes[x].cdcliente == page.route.params.cdcliente) {
                                htmlConferencia = ``;
                                /*usa o embalagem liberacao pois só pode entregar o que está no caminhão
                                 * no retorno usa o do cliente, pois só pode pegar embalagem que o cliente tenha*/
                                if (arrEmbalagens) {
                                    for (y in arrEmbalagens) {
                                        vlSaida = 0;
                                        vlEntrada = 0;
                                        txobs = "";
                                        if (arrClientes[x].embalagens) {
                                            for (z in arrClientes[x].embalagens) {
                                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                                    if (!Number(arrClientes[x].embalagens[z].vlsaida)) {
                                                        arrClientes[x].embalagens[z].vlsaida = 0;
                                                    }
                                                    if (!Number(arrClientes[x].embalagens[z].vlentrada)) {
                                                        arrClientes[x].embalagens[z].vlentrada = 0;
                                                    }
                                                    if (arrClientes[x].embalagens[z].txobs && arrClientes[x].embalagens[z].txobs != 'undefined') {
                                                        txobs = arrClientes[x].embalagens[z].txobs;
                                                    }
                                                    vlSaida = Number(arrClientes[x].embalagens[z].vlsaida);
                                                    vlEntrada = Number(arrClientes[x].embalagens[z].vlentrada);
                                                }
                                            }
                                        }

                                        htmlConferencia += `<ul class="background-gray mt16 ml16  mr16">
                                                                <li class="item-content item-input  li-header-custom">
                                                                    <div class="item-inner">
                                                                        <div class="item-title-row">
                                                                            <div class="item-title fontweight600">${arrEmbalagens[y].txembalagem}</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-content item-input">
                                                                    <div class="item-inner">
                                                                        <div class="item-title item-label pt12 pb12 wautoFL">Caixas com pão</div>
                                                                        <div class="item-input-wrap w100FL bolder-all ml16">
                                                                            <input class="background-input-gray fontweightbold pl16" type="number" step="1"  id="vlentrada${arrEmbalagens[y].cdembalagem}"  name="vlentrada${arrEmbalagens[y].cdembalagem}"  value="${vlEntrada}">
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-content item-input">
                                                                    <div class="item-inner">
                                                                        <div class="item-title item-label pt12 pb12 wautoFL">Caixas vazias</div>
                                                                        <div class="item-input-wrap w100FL bolder-all ml16">
                                                                            <input class="background-input-gray fontweightbold pl16" type="number" step="1"  id="vlsaida${arrEmbalagens[y].cdembalagem}"  name="vlsaida${arrEmbalagens[y].cdembalagem}"  value="${vlSaida}">
                                                                        </div>
                                                                    </div>
                                                                </li> 
                                                                <li class="item-content item-input">
                                                                    <div class="item-inner">
                                                                        <div class="item-title item-label pb16">Observações</div>
                                                                        <div class="item-input-wrap">
                                                                            <textarea  id="txobs${arrEmbalagens[y].cdembalagem}" name="txobs${arrEmbalagens[y].cdembalagem}"  class="background-input-gray"  >${txobs}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>`;
                                    }
                                }
                                htmlConferencia += `<div class="block">
                                                        <p class="row">
                                                            <a id="confirmaConferencia" href="javascript:confirmaConferencia(${arrClientes[x].cdcliente});" class="col button button-big button-fill background-color-custom">Confirmar</a>
                                                        </p>
                                                        <p class="row">
                                                            <a id="confirmandoConferencia" class="col button button-big button-fill background-color-custom hide">
                                                                <span class="preloader">
                                                                    <span class="preloader-inner">
                                                                        <span class="preloader-inner-gap"></span>
                                                                        <span class="preloader-inner-left">
                                                                            <span class="preloader-inner-half-circle"></span>
                                                                        </span>
                                                                        <span class="preloader-inner-right">
                                                                            <span class="preloader-inner-half-circle"></span>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </a>
                                                        </p>
                                                    </div>`;
                                if (!flHTML) {
                                    flHTML = 1;
                                    $("#recipConferencia").after(htmlConferencia);
                                    $(".loadRemove").remove();
                                }
                            }
                        }
                    }
                }

                if (page.name == 'consulta-saldo-promotor') {
                    flHTML = 0;
                    $("#recipClienteSaldo").html($(".page-previous").find('#topoCliente').html());
                    if (arrClientes) {
                        for (x in arrClientes) {
                            if (arrClientes[x].cdcliente == page.route.params.cdcliente) {
                                htmlClienteSaldo = ``;
                                /*usa o embalagem liberacao pois só pode entregar o que está no caminhão
                                 * no retorno usa o do cliente, pois só pode pegar embalagem que o cliente tenha*/
                                if (arrEmbalagens) {
                                    for (y in arrEmbalagens) {
                                        vlSaida = 0;
                                        vlEntrada = 0;
                                        vlSaldo = 0;
                                        vlSaldoF = 0;
                                        if (arrClientes[x].embalagens) {
                                            for (z in arrClientes[x].embalagens) {
                                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                                    if (!Number(arrClientes[x].embalagens[z].vlsaida)) {
                                                        arrClientes[x].embalagens[z].vlsaida = 0;
                                                    }
                                                    if (!Number(arrClientes[x].embalagens[z].vlentrada)) {
                                                        arrClientes[x].embalagens[z].vlentrada = 0;
                                                    }
                                                    if (!arrClientes[x].embalagens[z].vlsaldo) {
                                                        arrClientes[x].embalagens[z].vlsaldo = 0;
                                                    }
                                                    // //logObject(arrClientes[x].embalagens[z]);
                                                    vlSaida = Number(arrClientes[x].embalagens[z].vlsaida);
                                                    vlEntrada = Number(arrClientes[x].embalagens[z].vlentrada);
                                                    vlSaldo = Number(arrClientes[x].embalagens[z].vlsaldo);
                                                    vlSaldoF = (vlEntrada + vlSaida);
                                                }
                                            }
                                        }
                                        htmlClienteSaldo += `<ul class="background-gray ml16 mt16 mr16">
                 <li class="item-content item-input">
                     <div class="item-inner">
                         <div class="item-title-row">
                             <div class="item-title fontweight600">${arrEmbalagens[y].txembalagem}</div>
                         </div>
                     </div>
                 </li>
                 <li class="item-content item-input">
                     <div class="item-inner">
                         <div class="item-title item-label pb16">Saldo Amplus</div>
                         <div class="item-input-wrap">
                             <input class="background-none " type="text" readonly value="${vlSaldo}">
                         </div>
                     </div>
                     <div class="item-inner">
                         <div class="item-title item-label pb16">Saldo Conferência</div>
                         <div class="item-input-wrap">
                             <input class="background-none " type="text" readonly value="${vlSaldoF}">
                         </div>
                     </div>
                 </li> 
             </ul>`;
                                    }
                                }
                                if (!flHTML) {
                                    flHTML = 1;
                                    $("#recipClienteSaldo").after(htmlClienteSaldo);
                                    $(".loadRemove").remove();
                                }
                            }
                        }
                    }
                }

                if (page.name == 'consulta-saldo') {
                    flHTML = 0;
                    $("#recipClienteSaldo").html($(".page-previous").find('#topoCliente').html());
                    if (arrClientes) {
                        for (x in arrClientes) {
                            if (arrClientes[x].cdcliente == page.route.params.cdcliente) {
                                htmlClienteSaldo = ``;
                                /*usa o embalagem liberacao pois só pode entregar o que está no caminhão
                                 * no retorno usa o do cliente, pois só pode pegar embalagem que o cliente tenha*/
                                if (arrEmbalagens) {
                                    for (y in arrEmbalagens) {
                                        vlSaida = 0;
                                        vlEntrada = 0;
                                        vlSaldo = 0;
                                        vlSaldoF = 0;
                                        if (arrClientes[x].embalagens) {
                                            for (z in arrClientes[x].embalagens) {
                                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                                    if (!Number(arrClientes[x].embalagens[z].vlsaida)) {
                                                        arrClientes[x].embalagens[z].vlsaida = 0;
                                                    }
                                                    if (!Number(arrClientes[x].embalagens[z].vlentrada)) {
                                                        arrClientes[x].embalagens[z].vlentrada = 0;
                                                    }
                                                    if (!arrClientes[x].embalagens[z].vlsaldo) {
                                                        arrClientes[x].embalagens[z].vlsaldo = 0;
                                                    }
                                                    // //logObject(arrClientes[x].embalagens[z]);
                                                    vlSaida = Number(arrClientes[x].embalagens[z].vlsaida);
                                                    vlEntrada = Number(arrClientes[x].embalagens[z].vlentrada);
                                                    vlSaldo = Number(arrClientes[x].embalagens[z].vlsaldo);
                                                    vlSaldoF = vlSaldo + (vlEntrada - vlSaida);
                                                }
                                            }
                                        }
                                        htmlClienteSaldo += `<ul class="background-gray ml16  mr16">
                 <li class="item-content item-input">
                     <div class="item-inner">
                         <div class="item-title-row">
                             <div class="item-title fontweight600">${arrEmbalagens[y].txembalagem}</div>
                         </div>
                     </div>
                 </li>
                 <li class="item-content item-input">
                     <div class="item-inner">
                         <div class="item-title item-label pb16">Saldo Inicial</div>
                         <div class="item-input-wrap">
                             <input class="background-none " type="text" readonly value="${vlSaldo}">
                         </div>
                     </div>
                     <div class="item-inner">
                         <div class="item-title item-label pb16">Saldo Final</div>
                         <div class="item-input-wrap">
                             <input class="background-none " type="text" readonly value="${vlSaldoF}">
                         </div>
                     </div>
                 </li>
                 <li class="item-content item-input item-input-focused">
                     <div class="item-inner text-color-green">
                         <div class="item-title item-label pt12 pb12 wautoFL">Quantidade Entrega</div>
                         <div class="item-input-wrap">
                             <input class="background-input-gray" type="number" step="1" readonly value="${vlEntrada}">
                         </div>
                     </div>
                     <div class="item-inner text-color-red">
                         <div class="item-title item-label pt12 pb12 wautoFL">Quantidade Retorno</div>
                         <div class="item-input-wrap">
                             <input class="background-input-gray" type="number" step="1" readonly value="${vlSaida}">
                         </div>
                     </div>
                 </li>
             </ul>`;
                                    }
                                }
                                if (!flHTML) {
                                    flHTML = 1;
                                    $("#recipClienteSaldo").after(htmlClienteSaldo);
                                    $(".loadRemove").remove();
                                }
                            }
                        }
                    }
                }

                if (page.name == 'transferencia-caminhao') {
                    montaTopoLiberacao();
                    retornaTodosMotoristasVeiculoLiberacao();
                    $$('.ptr-content').on('ptr:refresh', function (e) {
                        mainView.router.navigate('/transferencia-caminhao', {
                            reloadCurrent: true, //troca a pagina atual por essa remove do historico
                            reloadPrevious: false, // troca a anterior por essa
                            reloadAll: false // zera a rota e começa de novo, usar no logout
                        })
                    })
                }

                if (page.name == 'transferencias') {
                    montaTopoLiberacao();
                }

                if (page.name == 'transferencia-enviada') {
                    montaTopoLiberacao();
                    listaHistoricoTransferenciasLiberacao(1)
                }

                if (page.name == 'transferencia-recebida') {
                    montaTopoLiberacao();
                    listaHistoricoTransferenciasLiberacao(2)
                }
            }
            pgInit = 0;
            $("input").focus(function () {
                vlInputOld = $(this).val();
                if ($(this).val() == 0) {
                    $(this).val('')
                }
            })

            $('input').keyup(function (e) { 
                if(e.which == 13){
                        
                } 
                
                if ($(this).attr('type') == 'number') {
                    if(e.which == 8){
                        vlInputOld = "";
                    } 
                    vlInput = $(this).val(); 
                    vlInputC = parseInt(parseFloat(vlInput));
                    if(!isNaN(vlInputC)){ 
                        vlInputOld = vlInputC; 
                        $(this).val(vlInputC);
                    }else{
                        $(this).val(vlInputOld); 
                    } 
                } 
            });   

            if (page.name == 'transferencias') {
                if (temInternet()) {
                    $("#btBaixarTransf").removeClass('hide')
                } else {
                    $("#btBaixarTransf").addClass('hide')
                }
            }

        }
    }
});

var dialog = app.dialog.progress("Verificando dispositivo", );

$$(document).on('deviceready', function () {
    createDB();
//    var permissions = cordova.plugins.permissions;
//    var listPermission = [permissions.ACCESS_COARSE_LOCATION, permissions.ACCESS_FINE_LOCATION];
//    permissions.hasPermission(listPermission, function (status) {
//        if (status.hasPermission) {
//            alert("Yes :D ");
//        } else {
//            alert("No :( ");
//        }
//        if (!status.hasPermission) {
//            permissions.requestPermissions(
//                    listPermission,
//                    function (status) {
//                        if (!status.hasPermission)
//                            alert("No2 :( ");
//                    },
//                    function (e) {
//                        alert("ePermission2")
//                        //logObject(e)
//                    });
//        }
//    },
//            function (e) {
//                alert("ePermission")
//                //logObject(e)
//            });

    pegarCoordenadas();
    intervalCoord = setInterval(function () {
        pegarCoordenadas();
    }, 60000);


    document.addEventListener("backbutton", function (e) {
        voltarPagina()
    }, false);

    uuid = device.uuid;
    system = device.platform;


    dialog.setText(uuid);

    if(temInternet()){
        vlFazInterval = 1;
        setInterval(function () {
            if (vlFazInterval == 1) {
                vlFazInterval = 0;

                //deixar um load e validar no servidor se uuid liberado
                jQuery.support.cors = true;
                $.ajax({
                    type: "POST",
                    crossDomain: true,
                    dataType: 'json',
                    url: "http://177.220.156.147/",
                    data: {acao: 'retornaUUIDValido', uuid: uuid},
                    success: function (data) {
                        if (data.ID_DEVICE) {
                            verificaLogado();
                        } else {

                        }
                    }, error: function (xhr) {
                        navigator.app.exitApp();
                    }
                })

                setInterval(function () {
                    if (txLiberacao) {
                        retornaHistoricoTransferenciasLiberacao();
                    }
                }, 300000);

                intervalSinc = setInterval(function () {
                    if (cdMotorista || cdPromotor) {
                        retornaClienteEmbalagemEntrada();
                    }
                }, 1000);
            }
        }, 3000);
    }else{
        verificaLogado();
    }
});

 

window.addEventListener('native.keyboardshow', function (e) {
    setTimeout(function () {
        document.activeElement.scrollIntoViewIfNeeded();
    }, 100);
});

window.addEventListener('native.keyboardhide', function (e) {
    setTimeout(function () {
        document.activeElement.scrollIntoViewIfNeeded();
    }, 100);
});

// Init/Create main view
var mainView = app.views.create('.view-main', {
    url: '/',
    dynamicNavbar: true,
    xhrCache: true,
    domCache: true
});

