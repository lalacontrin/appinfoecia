routes = [
    {
        path: '/',
        url: './index.html',
    },
    {
        path: '/inicial',
        url: './pages/inicial.html',
    },
    {
        path: '/inicial-promotor',
        url: './pages/inicial-promotor.html',
    },
    {
        path: '/iniciar-conferencia/:cdcliente',
        url: './pages/iniciar-conferencia.html',
    },
    {
        path: '/carregar-dados',
        url: './pages/carregar-dados.html',
    },
    {
        path: '/lista-clientes/:txbusca',
        url: './pages/lista-clientes.html',
    },
    {
        path: '/lista-clientes',
        url: './pages/lista-clientes.html',
    },
    {
        path: '/cliente/:cdcliente',
        url: './pages/cliente.html',
    },
    {
        path: '/cliente',
        url: './pages/cliente.html',
    },
    {
        path: '/entrega-caixa/:cdcliente',
        url: './pages/entrega-caixa.html',
    },
    {
        path: '/retorno-caixa/:cdcliente',
        url: './pages/retorno-caixa.html',
    },
    {
        path: '/consulta-saldo/:cdcliente',
        url: './pages/consulta-saldo.html',
    },
    {
        path: '/consulta-saldo-promotor/:cdcliente',
        url: './pages/consulta-saldo-promotor.html',
    },
    {
        path: '/entrega-caixa',
        url: './pages/entrega-caixa.html',
    },
    {
        path: '/retorno-caixa',
        url: './pages/retorno-caixa.html',
    },
    {
        path: '/consulta-saldo',
        url: './pages/consulta-saldo.html',
    },
    {
        path: '/tranferencias',
        url: './pages/tranferencias.html',
    },
    {
        path: '/transferencia-caminhao',
        url: './pages/transferencia-caminhao.html',
    },
    {
        path: '/transferencia-enviada',
        url: './pages/transferencia-enviada.html',
    },
    {
        path: '/transferencia-recebida',
        url: './pages/transferencia-recebida.html',
    },
    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html',
    },
];
