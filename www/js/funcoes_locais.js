function  montaHmtlLiberacao(arrLiberacao) {

    htmlDadosLiberacao = ``;
    if (arrLiberacao) {
        for (y in arrLiberacao['embalagens']) {
            insereLiberacaoEmbalagem(arrLiberacao.cdliberacao, arrLiberacao['embalagens'][y].cdembalagem, arrLiberacao['embalagens'][y].txembalagem, arrLiberacao['embalagens'][y].vlcaixascheias, arrLiberacao['embalagens'][y].vlcaixasvazias)

            htmlDadosLiberacao += `<ul class="background-gray ml16  mr16">
                <li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600">${arrLiberacao['embalagens'][y].txembalagem}</div>
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0">
                        <div class="item-title item-label pb16 ">Quantidade de Caixas</div>
                        <div class="item-input-wrap">
                            <input class="background-none " type="text" readonly value="${Number(arrLiberacao['embalagens'][y].vlcaixascheias)}">
                        </div>
                    </div>
                </li>
            </ul>`;

        }
    }
    htmlDadosLiberacao += `<div class="block">
                <p class="row">
                    <a  id="confirmarLiberacao"  href="javascript:confirmarLiberacao();" class="col button button-big button-fill background-color-custom">Confirmar</a>
                </p> 
                <p class="row">
                    <a id="confirmandoLiberacao" class="col button button-big button-fill background-color-custom hide">
                        <span class="preloader">
                            <span class="preloader-inner">
                                <span class="preloader-inner-gap"></span>
                                <span class="preloader-inner-left">
                                    <span class="preloader-inner-half-circle"></span>
                                </span>
                                <span class="preloader-inner-right">
                                    <span class="preloader-inner-half-circle"></span>
                                </span>
                            </span>
                        </span>
                    </a>
                </p>
            </div>`;


    $('.page-current').find("#spanLiberacao").html(arrLiberacao.cdliberacao);
    $('.page-current').find("#beforeCarregarDados").after(htmlDadosLiberacao);
    $('.page-current').find('#dados_carga').removeClass('hide');
    $('.page-current').find("#carregandoDados").addClass('hide');
    $('.page-current').find("#buscarLiberação").addClass('hide')
    $('.page-current .page-content').animate({scrollTop: ($(".page-current #beforeCarregarDados").offset().top + $('.page-current .page-content').scrollTop()) - 100}, 500);

}

function confirmarLiberacao() {
    $('.page-current').find("#confirmarLiberacao").remove();
    $('.page-current').find("#confirmandoLiberacao").removeClass('hide');
    insereLiberacao(arrLiberacao.cdliberacao, cdMotorista, txMotorista, cdVeiculo, txVeiculo, "");
    if (arrLiberacao) {
        for (y in arrLiberacao['embalagens']) {
            insereLiberacaoEmbalagem(arrLiberacao.cdliberacao, arrLiberacao['embalagens'][y].cdembalagem, arrLiberacao['embalagens'][y].txembalagem, arrLiberacao['embalagens'][y].vlcaixascheias, arrLiberacao['embalagens'][y].vlcaixasvazias)
        }
    }

    flFazDirectLiberacao = 1;
    setInterval(function () {
        //alert("flFazDirect")
        if (flFazDirectLiberacao == 1) {
            flFazDirectLiberacao = 0;
            mainView.router.navigate('/inicial', {
                reloadCurrent: true, //troca a pagina atual por essa remove do historico
                reloadPrevious: false, // troca a anterior por essa
                reloadAll: false // zera a rota e começa de novo, usar no logout
            })
        }
    }, 6000);

}

function confirmaConferencia(cdcliente) {
    $('.page-current').find("#confirmaConferencia").addClass('hide');
    $('.page-current').find("#confirmandoConferencia").removeClass('hide');
    dtentrada = retornaHojeC();
    if (arrClientes) {
        for (x in arrClientes) {
            if (arrClientes[x].cdcliente == cdcliente) {
                if (arrEmbalagens) {
                    for (y in arrEmbalagens) {
                        if ($(`#vlentrada${arrEmbalagens[y].cdembalagem}`).val()) {
                            vlEntrada = $(`#vlentrada${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            vlEntrada = 0;
                        }
                        if ($(`#vlsaida${arrEmbalagens[y].cdembalagem}`).val()) {
                            vlSaida = $(`#vlsaida${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            vlSaida = 0;
                        }
                        if ($(`#txobs${arrEmbalagens[y].cdembalagem}`).val()) {
                            txobs = $(`#txobs${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            txobs = "";
                        }
                        tem = 0;
                        cont = 0;
                        if (arrClientes[x].embalagens) {
                            for (z in arrClientes[x].embalagens) {
                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                    tem = 1;
                                    atualizaClienteEmbalagemEntrada(cdcliente, arrEmbalagens[y].cdembalagem, vlEntrada, vlSaida, dtentrada, txobs)
                                    arrClientes[x].embalagens[z].vlentrada = vlEntrada;
                                    arrClientes[x].embalagens[z].vlsaida = vlSaida;
                                    arrClientes[x].embalagens[z].txobs = txobs;
                                }
                                cont = z + 1;
                            }
                        } else {
                            arrClientes[x].embalagens = [];
                        }
                        if (!tem && vlEntrada > 0) {
                            //console.log('tem')
                            insereSomenteClienteEmbalagem('', cdcliente, arrEmbalagens[y].cdembalagem, arrEmbalagens[y].txembalagem, 0, 0)
                            insereClienteEmbalagemEntrada(cdcliente, arrEmbalagens[y].cdembalagem, vlEntrada, vlSaida, dtentrada, 0, txobs);
                            //logObject('arrClientes[x]')
                            //logObject(arrClientes[x])
                            //console.log("\n***\n")
                            //logObject(arrClientes[x].embalagens)
                            //console.log("\n***\n")
                            //logObject(arrClientes[x].embalagens.length)
                            //console.log("\n**++++++++++++++*\n")
                            if (arrClientes[x].embalagens.length) {
                                cont = arrClientes[x].embalagens.length;
                            }
                            arrClientes[x].embalagens[cont] = [];
                            arrClientes[x].embalagens[cont].cdembalagem = arrEmbalagens[y].cdembalagem;
                            arrClientes[x].embalagens[cont].txembalagem = arrEmbalagens[y].txembalagem;
                            arrClientes[x].embalagens[cont].vlsaldo = 0;
                            arrClientes[x].embalagens[cont].vlentrada = vlEntrada;
                            arrClientes[x].embalagens[cont].vlsaida = vlSaida;
                            arrClientes[x].embalagens[cont].txobs = txobs;
                            arrClientes[x].embalagens[cont].flsincronizado = 0;
                        }
                    }
                }
                //logObject("arrClientes[x]")
                //logObject(arrClientes[x])
                voltarPagina();
                return;
            }
        }
    }
}

function confirmarEntrega(cdcliente) {
    $('.page-current').find("#confirmarEntrega").addClass('hide');
    $('.page-current').find("#confirmandoEntrega").removeClass('hide');
    dtentrada = retornaHojeC();
    if (arrClientes) {
        for (x in arrClientes) {
            if (arrClientes[x].cdcliente == cdcliente) {
                if (arrEmbalagens) {
                    for (y in arrEmbalagens) {
                        if ($(`#vlentrada${arrEmbalagens[y].cdembalagem}`).val()) {
                            vlEntrada = $(`#vlentrada${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            vlEntrada = 0;
                        }
                        if ($(`#txobs${arrEmbalagens[y].cdembalagem}`).val()) {
                            txobs = $(`#txobs${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            txobs = "";
                        }
                        tem = 0;
                        cont = 0;
                        if (arrClientes[x].embalagens) {
                            for (z in arrClientes[x].embalagens) {
                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
//                                    console.log("Sim ")
                                    tem = 1;
                                    arrEmbalagens[y].vlcxvazia = Number(arrEmbalagens[y].vlcxvazia);
                                    if (!Number(arrClientes[x].embalagens[z].vlentrada)) {
                                        arrClientes[x].embalagens[z].vlentrada = 0;
                                    }
                                    if (!Number(arrClientes[x].embalagens[z].vlsaida)) {
                                        arrClientes[x].embalagens[z].vlsaida = 0;
                                    }
                                    if (vlEntrada != arrClientes[x].embalagens[z].vlentrada) {
                                        if (Number(vlEntrada) > Number(arrClientes[x].embalagens[z].vlentrada)) {
                                            dif = Number(vlEntrada) - Number(arrClientes[x].embalagens[z].vlentrada);
                                        } else {
                                            dif = Number(arrClientes[x].embalagens[z].vlentrada) - Number(vlEntrada);
                                        }
                                        arrEmbalagens[y].vlcxvazia -= Number(dif);
                                    }
                                    atualizaClienteEmbalagemEntrada(cdcliente, arrEmbalagens[y].cdembalagem, vlEntrada, arrClientes[x].embalagens[z].vlsaida, dtentrada, txobs)
                                    arrClientes[x].embalagens[z].vlentrada = vlEntrada;
                                    arrClientes[x].embalagens[z].txobs = txobs;
                                    updateEmbalagemVazia(arrEmbalagens[y].cdembalagem, arrEmbalagens[y].vlcxvazia);
                                }
                                cont = z + 1;
                            }
                        } else {
                            arrClientes[x].embalagens = [];
                        }
//                        console.log("Tem: " + tem)
                        if (!tem && vlEntrada > 0) {
//                            console.log("Não ")
                            insereSomenteClienteEmbalagem('', cdcliente, arrEmbalagens[y].cdembalagem, arrEmbalagens[y].txembalagem, 0, 0)
                            insereClienteEmbalagemEntrada(cdcliente, arrEmbalagens[y].cdembalagem, vlEntrada, 0, dtentrada, 0, txobs);
                            if (arrClientes[x].embalagens.length) {
                                cont = arrClientes[x].embalagens.length;
                            }
                            arrClientes[x].embalagens[cont] = [];
                            arrClientes[x].embalagens[cont].cdembalagem = arrEmbalagens[y].cdembalagem;
                            arrClientes[x].embalagens[cont].txembalagem = arrEmbalagens[y].txembalagem;
                            arrClientes[x].embalagens[cont].vlsaldo = 0;
                            arrClientes[x].embalagens[cont].vlentrada = vlEntrada;
                            arrClientes[x].embalagens[cont].vlsaida = 0;
                            arrClientes[x].embalagens[cont].txobs = txobs;
                            arrClientes[x].embalagens[cont].flsincronizado = 0;

                            arrEmbalagens[y].vlcxvazia = Number(arrEmbalagens[y].vlcxvazia);
                            arrEmbalagens[y].vlcxvazia -= Number(vlEntrada);
                            updateEmbalagemVazia(arrEmbalagens[y].cdembalagem, arrEmbalagens[y].vlcxvazia)
                        }
                    }
                }
                vlVoltarAgora = 1;
                setInterval(function () {
                    if (vlVoltarAgora == 1) {
                        vlVoltarAgora = 0;
                        voltarPagina();
                    }
                }, 3000);
                return;
            }
        }
    }
}

function confirmaRetorno(cdcliente) {
    $('.page-current').find("#confirmaRetorno").addClass('hide');
    $('.page-current').find("#confirmandoRetorno").removeClass('hide');
    dtentrada = retornaHojeC();
    if (arrClientes) {
        for (x in arrClientes) {
            if (arrClientes[x].cdcliente == cdcliente) {
                if (arrEmbalagens) {
                    for (y in arrEmbalagens) {
                        if ($(`#vlsaida${arrEmbalagens[y].cdembalagem}`).val()) {
                            vlSaida = $(`#vlsaida${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            vlSaida = 0;
                        }
                        if ($(`#txobs${arrEmbalagens[y].cdembalagem}`).val()) {
                            txobs = $(`#txobs${arrEmbalagens[y].cdembalagem}`).val();
                        } else {
                            txobs = "";
                        }
                        tem = 0;
                        cont = 0;
                        if (arrClientes[x].embalagens) {
                            for (z in arrClientes[x].embalagens) {
                                if (arrClientes[x].embalagens[z].cdembalagem == arrEmbalagens[y].cdembalagem) {
                                    tem = 1;
                                    arrEmbalagens[y].vlcxvazia = Number(arrEmbalagens[y].vlcxvazia);
                                    if (!Number(arrClientes[x].embalagens[z].vlentrada)) {
                                        arrClientes[x].embalagens[z].vlentrada = 0;
                                    }
                                    if (!Number(arrClientes[x].embalagens[z].vlsaida)) {
                                        arrClientes[x].embalagens[z].vlsaida = 0;
                                    }
                                    if (vlSaida != arrClientes[x].embalagens[z].vlsaida) {
                                        if (Number(vlSaida) > Number(arrClientes[x].embalagens[z].vlsaida)) {
                                            dif = Number(vlSaida) - Number(arrClientes[x].embalagens[z].vlsaida);
                                        } else {
                                            dif = Number(arrClientes[x].embalagens[z].vlsaida) - Number(vlSaida);
                                        }
                                        arrEmbalagens[y].vlcxvazia += dif;
                                    }
                                    atualizaClienteEmbalagemEntrada(cdcliente, arrEmbalagens[y].cdembalagem, arrClientes[x].embalagens[z].vlentrada, vlSaida, dtentrada, txobs)
                                    arrClientes[x].embalagens[z].vlsaida = vlSaida;
                                    arrClientes[x].embalagens[z].txobs = txobs;
                                    updateEmbalagemVazia(arrEmbalagens[y].cdembalagem, arrEmbalagens[y].vlcxvazia)
                                }
                                cont = z + 1;
                            }
                        } else {
                            arrClientes[x].embalagens = [];
                        }
                        if (!tem && vlSaida > 0) {
                            insereSomenteClienteEmbalagem('', cdcliente, arrEmbalagens[y].cdembalagem, arrEmbalagens[y].txembalagem, 0, 0)
                            insereClienteEmbalagemEntrada(cdcliente, arrEmbalagens[y].cdembalagem, 0, vlSaida, dtentrada, 0, txobs)
                            if (arrClientes[x].embalagens.length) {
                                cont = arrClientes[x].embalagens.length;
                            }
                            arrClientes[x].embalagens[cont] = [];
                            arrClientes[x].embalagens[cont].cdembalagem = arrEmbalagens[y].cdembalagem;
                            arrClientes[x].embalagens[cont].txembalagem = arrEmbalagens[y].txembalagem;
                            arrClientes[x].embalagens[cont].vlsaldo = 0;
                            arrClientes[x].embalagens[cont].vlentrada = 0;
                            arrClientes[x].embalagens[cont].vlsaida = vlSaida;
                            arrClientes[x].embalagens[cont].txobs = txobs;
                            arrClientes[x].embalagens[cont].flsincronizado = 0;

                            arrEmbalagens[y].vlcxvazia = Number(arrEmbalagens[y].vlcxvazia);
                            arrEmbalagens[y].vlcxvazia += Number(vlSaida);
                            updateEmbalagemVazia(arrEmbalagens[y].cdembalagem, arrEmbalagens[y].vlcxvazia)
                        }
                    }
                }
                vlVoltarAgora = 1;
                setInterval(function () {
                    if (vlVoltarAgora == 1) {
                        vlVoltarAgora = 0;
                        voltarPagina();
                    }
                }, 3000);
                return;
            }
        }
    }
}

function montaHtmlTranferencia(arrTransf) {
    //console.log('aki')
    //logObject(arrTransf)
    if (arrTransf) {
        htmlListTransf = `<ul>`;
        for (x in arrTransf) {
            //console.log('for')
            dttransferencia = arrTransf[x].dttransferencia;
            dttransferenciaEdit = (dttransferencia).substr(8, 2) + "/" + (dttransferencia).substr(5, 2) + "/" + (dttransferencia).substr(0, 4) + " - " + (dttransferencia).substr(11, 5);
            arrTransf[x].vlquantidade = Number(arrTransf[x].vlquantidade);

            txLiberacaoORIG = "";
            if (arrTransf[x].fltipo == 2) {
                txLiberacaoORIG = `<div class="item-subtitle item-search-result">Liberação de Origem: <b>${arrTransf[x].cdliberacao}</b></div>`
            } else {
                txLiberacaoORIG = `<div class="item-subtitle item-search-result">Liberação de Destino: <b>${arrTransf[x].cdliberacao}</b></div>`
            }

            //console.log('for3')
            htmlListTransf += `<li>
                                    <div class="item-content pl0">
                                        <div class="item-inner">
                                            <div class="item-title-row">
                                                <div class="item-title item-search-result fontweight600">${dttransferenciaEdit}</div>
                                            </div>
                                            ${txLiberacaoORIG}
                                            <div class="item-subtitle item-search-result">${arrTransf[x].txveiculo} (${arrTransf[x].txmotorista})</div>
                                            <div class="item-subtitle">${arrTransf[x].vlquantidade} - ${arrTransf[x].txembalagem}</div>
                                        </div>
                                    </div>
                                </li>`;
        }
        htmlListTransf += `   </ul>`;
        //console.log('fim')
        $(".page-current #recipListTransf").addClass('list media-list inset mt0 ml0 mr0').html(htmlListTransf);
    } else {
        $(".page-current #recipListTransf").html(`<div class="block">
                                            <div class="block-inner">Nenhuma Transferência</div>
                                        </div>`);
    }
}

function encerrarMovimentacao() {
    if (temInternet()) {
        $(".btSincronizar").addClass('text-color-gray border-color-gray').removeClass('text-color-custom border-color-custom').removeAttr('href')
        $(".btSincronizarP").addClass('hide')
        app.dialog.preloader('Enviando...');
        contSincronizar = 0;
        arrSincronizar = arrSincronizar.slice(1, 1);
        clearInterval(intervalSinc)
        clearInterval(intervalCoord);
        retornaClienteEmbalagemEntradaSincronizar(3);
    } else {
        voltarPagina();
    }
}

function montaHTMLTransferirCaminhao(arrLiberacoes) {
    //console.log('123')
    if (arrLiberacoes) {
        selLiberacao = `<select id="cdliberacao_transferencia" name="cdliberacao_transferencia" >
                                <option value="">Selecione a Liberação</option>`;
        for (x in arrLiberacoes) {
            selLiberacao += `<option value="${arrLiberacoes[x].cdliberacao}">${arrLiberacoes[x].txveiculo} (${arrLiberacoes[x].txmotorista})</option>`;
        }
        selLiberacao += `</select>`;
        //console.log('456')
        htmlSelecionaTrasferir = `<li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600">Transferir para</div>
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0 w100p">
                        <div class="item-title item-label pb16">Veículo (Motorista)</div>
                        <div class="item-input-wrap">
                            ${selLiberacao}
                        </div>
                    </div>
                </li>`;
        //console.log('789')
        htmlEmbalagensTransferir = ``;
        if (arrEmbalagens) {
            for (x in arrEmbalagens) {
                vlcxvazia = arrEmbalagens[x].vlcxvazia;
                if (!Number(arrEmbalagens[x].vlcxvazia)) {
                    vlcxvazia = 0;
                }
                //logObject("vlcxvaziaIII");
                //logObject(vlcxvazia);
                vlcxvazia = Number(vlcxvazia);
                if (arrRecebidos) {
                    for (r in arrRecebidos) {
                        if (arrRecebidos[r].cdembalagem == arrEmbalagens[x].cdembalagem) {
                            if (!Number(arrRecebidos[r].vlquantidade)) {
                                arrRecebidos[r].vlquantidade = 0;
                            }
                            //console.log("arrRecebidos");
                            //logObject(arrRecebidos[r].vlquantidade);
                            vlcxvazia += Number(arrRecebidos[r].vlquantidade);
                        }
                    }
                }
                if (arrEnviados) {
                    for (r in arrEnviados) {
                        if (arrEnviados[r].cdembalagem == arrEmbalagens[x].cdembalagem) {
                            if (!Number(arrEnviados[r].vlquantidade)) {
                                arrEnviados[r].vlquantidade = 0;
                            }
                            //console.log("arrEnviados");
                            //logObject(arrEnviados[r].vlquantidade);
                            vlcxvazia -= Number(arrEnviados[r].vlquantidade);
                        }
                    }
                }
                //logObject("vlcxvazia");
                //logObject(vlcxvazia);
                htmlEmbalagensTransferir += `<ul class="background-gray mt16 ml16 mr16">
                  <li class="item-content item-input">
                      <div class="item-inner">
                          <div class="item-title-row">
                              <div class="item-title fontweight600">${arrEmbalagens[x].txembalagem}</div>
                          </div>
                      </div>
                  </li>
                  <li class="item-content item-input">
                      <div class="item-inner">
                          <div class="item-title item-label pt12 pb12 wautoFL">Quantidade Caixas</div>
                          <div class="item-input-wrap w100FL bolder-all ml16">
                              <input class="background-input-gray fontweightbold pl16" type="number" step="1" readonly  value="${vlcxvazia}">
                          </div>
                      </div>
                  </li>
                  <li class="item-content item-input">
                      <div class="item-inner">
                          <div class="item-title item-label pt12 pb12 wautoFL">Quantidade Transferir</div>
                          <div class="item-input-wrap w100FL bolder-all ml16">
                              <input class="background-input-gray fontweightbold pl16" type="number" step="1" id="vltransferir${arrEmbalagens[x].cdembalagem}" name="vltransferir${arrEmbalagens[x].cdembalagem}"  value="">
                          </div>
                      </div>
                  </li>
              </ul>`;
            }
        }
        //console.log('fffor')
        htmlEmbalagensTransferir += `<li class="item-content item-input mt32 ">
                    <div class="item-inner pt0 w100p">
                        <div class="item-title item-label">Senha</div>
                        <div class="item-input-wrap">
                            <input type="password" id="txsenhatranferencia" name="txsenhatranferencia" />
                        </div>
                    </div>
                </li>
                <div class="block">
                    <p class="row">
                        <a href="javascript:transferirCaminhao();" id="transferirCaminhao" class="col button button-big button-fill background-color-custom">Confirmar</a>
                    </p>
                    <p class="row">
                        <a id="transferindoCaminhao" class="col button button-big button-fill background-color-custom hide">
                            <span class="preloader ">
                                <span class="preloader-inner">
                                    <span class="preloader-inner-gap"></span>
                                    <span class="preloader-inner-left">
                                        <span class="preloader-inner-half-circle"></span>
                                    </span>
                                    <span class="preloader-inner-right">
                                        <span class="preloader-inner-half-circle"></span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </p>
              </div>`;
        //console.log('fim')
        $("#recipTransferirCaminhao").html(htmlSelecionaTrasferir).after(htmlEmbalagensTransferir);
    } else {
        htmlSelecionaTrasferir = `<li class="item-content item-input">
                    <div class="item-inner">
                        <div class="item-title-row">
                            <div class="item-title fontweight600">Transferir para</div>
                        </div>
                    </div>
                </li>
                <li class="item-content item-input">
                    <div class="item-inner pt0 w100p">
                        <div class="item-title item-label pb16">Nenhum veículo com liberação</div> 
                    </div>
                </li>`;

    }
}